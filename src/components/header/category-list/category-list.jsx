import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './category-list.scss';
import { useIntl } from 'react-intl';
import { NavLink, useHistory } from 'react-router-dom';

import { language } from '../../../redux/action/lang.action';
import { Menu, Divider } from 'antd';
import { useEffect } from 'react';
import { getCategory_req } from '../../../api/category-list/category.list.api';
import { useState } from 'react';

const CategoryList = ({ open, drop, setDrop, closeMenu }) => {
  const [data, setData] = useState([]);
  const history = useHistory();
  const dispatch = useDispatch();
  const intl = useIntl();

  const changeLanguage = (lang) => {
    dispatch(language(lang));
  };
  const categoryList = async () => {
    try {
      const categoryList = await getCategory_req();
      setData(categoryList);
    } catch (e) {}
  };
  useEffect(() => {
    categoryList();
  }, []);

  const slugRequest = async (slug) => {
    closeMenu();
    setDrop(!drop);
    history.push({
      pathname: '/slug/' + slug,
    });
  };
  if (!data?.data) {
    return null;
  }

  const { SubMenu } = Menu;

  return (
    <>
      <div className={`category_block${open ? ' open' : ''}`}>
        <>
          <div className="list-block sidenav-category">
            <div className="language_menu">
              <h4 className="language_name">{intl.messages.languages}</h4>
              <div className="language_img">
                <img
                  onClick={() => changeLanguage(1)}
                  src={require('../../../assets/images/Language/armenia.png')}
                  alt="ARMENIA"
                />
                <img
                  onClick={() => changeLanguage(2)}
                  src={require('../../../assets/images/Language/russia.png')}
                  alt="RUSSIA"
                />
              </div>
            </div>
            <div>
              <Menu mode="inline" className="menu-color">
                <SubMenu title={intl.messages.category}>
                  {data.data.map((item) => (
                    <Menu.ItemGroup>
                      {item.child.data && item.child.data.length ? (
                        <SubMenu
                          key={item.id}
                          title={item.name}
                          className="font-20 font-bold"
                        >
                          {item?.child?.data?.map((child) => (
                            <Menu.ItemGroup>
                              {child.child.data && child.child.data.length ? (
                                <SubMenu
                                  key={child.id}
                                  title={child.name}
                                  className=" font-18"
                                >
                                  {child?.child?.data?.map((item) => (
                                    <Menu.Item
                                      key={item.id}
                                      className=" font-16"
                                      onClick={() => slugRequest(item.slug)}
                                    >
                                      {item.name}
                                    </Menu.Item>
                                  ))}
                                </SubMenu>
                              ) : (
                                <Menu.Item
                                  key={child.id}
                                  className=" font-18"
                                  onClick={() => slugRequest(child.slug)}
                                >
                                  {child.name}
                                </Menu.Item>
                              )}
                            </Menu.ItemGroup>
                          ))}
                        </SubMenu>
                      ) : (
                        <Menu.Item
                          key={item.id}
                          className="font-20 font-bold"
                          onClick={() => slugRequest(item.slug)}
                        >
                          {item.name}
                        </Menu.Item>
                      )}
                    </Menu.ItemGroup>
                  ))}
                </SubMenu>
                <Divider className="my-0"></Divider>
                <SubMenu
                  key="gorcnakan1"
                  title={intl.messages.partners.toUpperCase()}
                  className="category-title"
                >
                  <Menu.Item key="1">{intl.messages.banks}</Menu.Item>
                  <Menu.Item key="2">{intl.messages.shops}</Menu.Item>
                  <Menu.Item key="3">{intl.messages.operators}</Menu.Item>
                </SubMenu>
                <Divider className="my-0"></Divider>
                <Menu.Item
                  open={open}
                  onClick={() => closeMenu()}
                  className="category-title"
                >
                  <NavLink to="/about_us" className="about-us-text">
                    {intl.messages.about_us.toUpperCase()}
                  </NavLink>
                </Menu.Item>
                <Divider className="my-0"></Divider>
                <Menu.Item onClick={() => closeMenu()}>
                  <NavLink to="/contact_us" className="contact-us-text">
                    {intl.messages.contact_us.toUpperCase()}
                  </NavLink>
                </Menu.Item>
                <Divider className="my-0"></Divider>
                <Menu.Item onClick={closeMenu}>
                  <NavLink to="/help-center" className="contact-us-text">
                    {intl.messages.help.toUpperCase()}
                  </NavLink>
                </Menu.Item>
                <Divider className="my-0"></Divider>
              </Menu>
            </div>
          </div>
          <div className="full-list" onClick={closeMenu}></div>
        </>
      </div>
    </>
  );
};

export default CategoryList;
