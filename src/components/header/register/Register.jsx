import React from 'react';
import '../header.scss';
import { NavLink } from 'react-router-dom';
import { useIntl } from 'react-intl';

const Registers = () => {
  const intl = useIntl();

  return (
    <ul className="register_link nav">
      <NavLink to="/registration" className="register">
        {intl.messages.registration.registration_title}
      </NavLink>
      <span className="reg_span ml-2 mr-2">/</span>
      <NavLink to="/login" className="register mr-2">
        {intl.messages.loginUser.login_title}
      </NavLink>
    </ul>
  );
};

export default Registers;
