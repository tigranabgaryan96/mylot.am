import React, { useState } from 'react';
import './header.scss';
import HeaderRight from './header-right/header-right';
import UserIcon from './user/user';
import { Navbar, Nav } from 'react-bootstrap';
import Search from './search/search';
import CategoryList from './category-list/category-list';
import Registers from './register/register';

const HeaderSignOut = (token) => {
  const [category, setCategory] = useState([]);

  return (
    <div className="header">
      <Navbar className="justify-content-between navbar-custom navbar">
        <Nav className="w-100 nav">
          <div className="menu">
            {/* <div className="menu-button" onClick={openCategory}>
              <span></span>
              <span></span>
              <span></span>
            </div> */}
            <div className="menu-text">
              <p>MENU</p>
            </div>

            <CategoryList data={category} setCategory={setCategory} />
          </div>
          <Search />
        </Nav>
        <Nav className="w-100 justify-content-end nav">
          {token.tokenUser ? <UserIcon /> : <Registers />}
          <HeaderRight token={token.tokenUser} />
        </Nav>
      </Navbar>
    </div>
  );
};

export default HeaderSignOut;
