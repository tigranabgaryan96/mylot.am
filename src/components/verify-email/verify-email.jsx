import React from 'react';
import './verify-email.scss';
import { useIntl } from 'react-intl';

const VerifyEmail = () => {
    const token = localStorage.token

    const intl = useIntl()

    return (
        <div className="verify">
            <div className="verify_block">
                <p className="verify_block_text">
                    {intl.messages.lot.success_lot}
                </p>
            </div>
        </div>
    )
}
export default VerifyEmail