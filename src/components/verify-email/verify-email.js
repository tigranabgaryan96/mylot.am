import React, { useEffect } from 'react';

import { useHistory, useLocation } from 'react-router-dom';
import { auth_verify } from '../../api/user-login/user.login.api';

const VerifyEmailToken = () => {
  const location = useLocation();
  const history = useHistory();
  const verify = location.pathname.slice(8);
  const authBaseUrl = async () => {
    try {
      const verifyUser = await auth_verify(verify);

      if (verifyUser) {
        history.push('/login');
      }
    } catch {}
  };
  useEffect(() => {
    authBaseUrl();
  }, []);
  return <></>;
};

export default VerifyEmailToken;
