import React from 'react';
import './unpide-product.scss';
import '../../../reset.scss';
import UnpideProductPagination from './unpide-product-pagination';
import { useIntl } from 'react-intl';
import UserProduct from '../../../app-components/user-product/user-product';
import { useHistory, Link } from 'react-router-dom';
import DeleteModal from '../../modal-popup/delete-modal/delete-modal';

const UnpideProduct = ({ data }) => {
  const intl = useIntl();
  const page = data?.meta?.pagination;

  let history = useHistory();

  function handleClick() {
    history.push(`/liveView`);
  }

  if (!data || !data.data) {
    return null;
  }

  return (
    <div className="unpide-product">
      <h3 className="text-center">{intl.messages.currents_lot}</h3>
      <div className="d-flex justify-content-between p-3 awaiting-banner">
        <div className="itemsFound">
          <p>
            <span className="mr-2">{intl.messages.Lot_lenght}</span>
            <span>{page.total}</span>
          </p>
        </div>
        <div className="page">
          <UnpideProductPagination page={page} />
        </div>
      </div>
      <div className="container">
        <div className="user-awaiting-block">
          {data.data.map((item) => (
            <div className="user-awaiting-list">
              <div className="bid-product-card">
                <UserProduct
                  id={item.id}
                  currency={item.currency}
                  title={item.title}
                  bidPrice={item.buy_now_price}
                  startPrice={item.start_price}
                  image={item.firstProductImage.data}
                  endDate={item.end_date}
                  participators={item.participators}
                  live={item.live}
                />
                <div className="awaiting-product-edit">
                  <div className="user-product-edit">
                    <Link to={`/edititem/${data.id}`}>
                      <img
                        src={require('../../../assets/images/UserProduct/edit.svg')}
                        alt="EDIT"
                      />
                    </Link>
                    <DeleteModal id={item.id} />
                  </div>
                  <div className="bid-now">
                    <button
                      type="button"
                      className="btn Sans_Bold addBid"
                      onClick={handleClick}
                    >
                      {intl.messages.go_live}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="d-flex justify-content-between p-3 awaiting-banner">
        <div className="itemsFound">
          <p>
            <span className="mr-2">{intl.messages.Lot_lenght}</span>
            <span>{page.total} </span>
          </p>
        </div>
        <div className="page">
          <UnpideProductPagination page={page} />
        </div>
      </div>
    </div>
  );
};

export default UnpideProduct;
