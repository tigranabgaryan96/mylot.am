import React from 'react';
import './awaiting-product.scss';
import '../../../reset.scss';
import AwaitingProductPagination from './awaiting-product-pagination/awaiting-product-pagination';
import { useIntl } from 'react-intl';
import UserProduct from '../../../app-components/user-product/user-product';
import { useHistory, Link } from 'react-router-dom';
import DeleteModal from '../../../components/modal-popup/delete-modal/delete-modal';

const AwaitingProduct = ({ data }) => {
  const intl = useIntl();
  const page = data?.meta?.pagination;
  let history = useHistory();
  function handleClick() {
    history.push(`/liveView`);
  }
  if (!data || !data.data) {
    return null;
  }

  return (
    <div className="awaiting-product">
      <h3 className="text-center">{intl.messages.expected_lots}</h3>
      <div className="d-flex justify-content-between p-3 awaiting-banner">
        <div className="itemsFound">
          <p>
            <span className="mr-2">{intl.messages.Lot_lenght}</span>
            <span>{page.total}</span>
          </p>
        </div>
        <div className="page">
          <AwaitingProductPagination page={page} />
        </div>
      </div>
      <div className="container">
        <div className="user-awaiting-block">
          {data?.data?.map((item) => (
            <div className="user-awaiting-list">
              <div className="bid-product-card">
                <UserProduct
                  id={item.id}
                  currency={item.currency}
                  title={item.title}
                  startPrice={item.start_price}
                  image={item.firstProductImage?.data}
                  bidPrice={item.buy_now_price}
                  startDate={item.start_date}
                  endDate={item.end_date}
                  participators={item.participators}
                  live={item.live}
                />
                <div className="awaiting-product-edit">
                  <div className="user-product-edit">
                    <Link to={`/edititem/${item.id}`}>
                      <img
                        src={require('../../../assets/images/UserProduct/edit.svg')}
                        alt="EDIT"
                      />
                    </Link>
                    <DeleteModal id={item.id} />
                  </div>
                  {/* <div className="bid-now">
                    <button
                      type="button"
                      className="btn Sans_Bold addBid"
                      onClick={handleClick}
                    >
                      {intl.messages.go_live}
                    </button>
                  </div> */}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="d-flex justify-content-between p-3 awaiting-banner">
        <div className="itemsFound">
          <p>
            <span className="mr-2">{intl.messages.Lot_lenght}</span>
            <span>{page.total} </span>
          </p>
        </div>
        <div className="page">
          <AwaitingProductPagination page={page} />
        </div>
      </div>
    </div>
  );
};

export default AwaitingProduct;
