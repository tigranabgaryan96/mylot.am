import React, { useState, useEffect } from 'react';
import './user-auction.scss';
import LiveProduct from './live-product/live-product';
import AwaitingProduct from './awaiting-product/awaiting-product';
import UnpideProduct from './upide-product/unpide-product';
import { Link, NavLink } from 'react-router-dom';
import { Card, Tabs, Tab } from 'react-bootstrap';

import {
  getUserLiveAuction_req,
  getUserAwaitingAuction_req,
  getUserFinished_req,
} from '../../api/user-aution/user.auction.api';

import { useIntl } from 'react-intl';
import NextRight from '../../assets/images/icon/right-arrow.svg';

const UserAuction = () => {
  const [liveProduct, setLiveProduct] = useState([]);
  const [awaitingProduct, setAwaitingProduct] = useState([]);
  const [finishedProduct, setFinishedProduct] = useState(null);

  const intl = useIntl();

  const upUserLiveProduct = async () => {
    const userLiveProductList = await getUserLiveAuction_req();

    setLiveProduct(userLiveProductList);
  };

  const upUserAwaitingProduct = async () => {
    const userAwaitingProductList = await getUserAwaitingAuction_req();

    setAwaitingProduct(userAwaitingProductList);
  };

  const userFinishedProduct = async () => {
    const response = await getUserFinished_req();
    setFinishedProduct(response);
  };

  useEffect(() => {
    upUserLiveProduct();
    upUserAwaitingProduct();
    userFinishedProduct();
  }, []);

  return (
    <React.Fragment>
      <div className="userAuction_container container">
        <nav className="nav py-4">
          <NavLink to="/home" className="text-dark mr-2">
            {intl.messages.main_page}
          </NavLink>
          <img src={NextRight} alt="RIGHT-ARROW" className="ml-1" />
          <NavLink to="/my-auction" className="active ml-2">
            {intl.messages.my_lot}
          </NavLink>
        </nav>
        <div className="py-3">
          <Tabs
            defaultActiveKey="Awaiting"
            transition={false}
            id="noanim-tab-example"
          >
            <Tab
              eventKey="Awaiting"
              title={
                <span className="tabTitle">
                  {intl.messages.my_lotPage.step_two}
                </span>
              }
            >
              <Card>
                <Card.Body className="d-flex justify-content-center">
                  <div className="addBtn">
                    <Link to="/addItem" className="AddLot">
                      <span>{intl.messages.my_lotPage.step_button}</span>
                    </Link>
                  </div>
                </Card.Body>
                <div className="card-product">
                  {awaitingProduct?.data?.length === 0 ? (
                    <div className="no_product_text">
                      {intl.messages.my_lotPage.step_text}
                    </div>
                  ) : (
                    <AwaitingProduct data={awaitingProduct} />
                  )}
                </div>
              </Card>
            </Tab>
            <Tab
              eventKey="Live"
              title={
                <span className="tabTitle">
                  {intl.messages.my_lotPage.step_one}
                </span>
              }
            >
              <Card>
                <Card.Body className="d-flex justify-content-center">
                  <div className="addBtn">
                    <Link to="/addItem" className="AddLot">
                      <span>{intl.messages.my_lotPage.step_button}</span>
                    </Link>
                  </div>
                </Card.Body>
                <div className="card-product">
                  {liveProduct?.data?.length === 0 ? (
                    <div className="no_product_text">
                      {intl.messages.my_lotPage.step_text_second}
                    </div>
                  ) : (
                    <LiveProduct data={liveProduct} />
                  )}
                </div>
              </Card>
            </Tab>
            <Tab
              eventKey="Upide"
              title={
                <span className="tabTitle">
                  {intl.messages.my_lotPage.step_three}
                </span>
              }
            >
              <Card>
                <Card.Body className="d-flex justify-content-center">
                  <div className="addBtn">
                    <Link to="/addItem" className="AddLot">
                      <span>{intl.messages.my_lotPage.step_button}</span>
                    </Link>
                  </div>
                </Card.Body>
                <div className="card-product">
                  {finishedProduct?.data?.length === 0 ? (
                    <div className="no_product_text">
                      {intl.messages.my_lotPage.step_text_third}
                    </div>
                  ) : (
                    <UnpideProduct data={finishedProduct} />
                  )}
                </div>
              </Card>
            </Tab>
          </Tabs>
        </div>
      </div>
    </React.Fragment>
  );
};

export default UserAuction;
