import React from "react";
import { useIntl} from 'react-intl';

export default ({ staticContext = {} }) => {
  staticContext.status = 404;
  const intl = useIntl();

  return (
    <div className="container">
      <h1 className="not_found_text d-flex align-items-center justify-content-center">
        {intl.messages.not_found}
      </h1>
    </div>
  );
};
