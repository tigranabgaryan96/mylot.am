import React from 'react';
import '../all-items.scss';
import PaginationItem from './pagination';
import CategoryProduct from './category-product';
import { useIntl } from 'react-intl';
import Preloader from '../../preloader/preloader';

const AuctionItem = (props, category) => {
  const intl = useIntl();

  if (!props.data) {
    return <Preloader />;
  }

  const dataName = props.props.category;
  const data = props.data.data;
  const page = props.data.meta;

  return (
    <div className="container-fluid AuctionItem clearfix card pt-4 pb-4">
      <h3 className="text-center categories_title mb-4">
        {dataName.data.name}
      </h3>
      <div className="d-flex justify-content-between categories_navigation">
        <div className="foundItems">
          <span>{intl.messages.Lot_lenght}</span>
          <span className="ml-2">{page.pagination.total}</span>
        </div>
        <div className="page">
          <PaginationItem setData={props.setData} page={page.pagination} />
        </div>
      </div>
      <CategoryProduct data={data} />
      <div className="d-flex justify-content-between categories_navigation mt-5">
        <div className="foundItems">
          <span>{intl.messages.Lot_lenght}</span>
          <span className="ml-2">{page.pagination.total}</span>
        </div>
        <div className="page">
          <PaginationItem setData={props.setData} page={page.pagination} />
        </div>
      </div>
    </div>
  );
};

export default AuctionItem;
