import React from 'react';
import './favorites.scss';
import '../../reset.scss';
import { NavLink } from 'react-router-dom';
import DeleteModal from '../modal-popup/delete-modal/delete-modal';
import { useIntl } from 'react-intl';
import UserProduct from '../../app-components/user-product/user-product';
import { useDispatch, useSelector } from 'react-redux';
import { favoritesProductList } from '../../redux/favorites.slice';

const Favorites = ({ data }) => {
  const intl = useIntl();
  const dispatch = useDispatch();

  const deleteItem = data.data.map((item) => item.id);

  if (!data || !data.data) {
    return null;
  }

  return (
    <React.Fragment>
      <div className="user-favorites-product">
        <nav className="nav py-4">
          <NavLink to="/home" className="text-dark mr-2">
            {intl.messages.main_page}
          </NavLink>
          <span>&gt;</span>
          <NavLink to="/favorites" className="active ml-2">
            {intl.messages.favorites.favorites_title}
          </NavLink>
        </nav>
        <div className="card mb-5">
          <div className="card-body">
            <h2 className="text-center header_blues">
              {intl.messages.favorites.favorites_title}
            </h2>
            <div className="user-favorites-block mt-5">
              {data.data.map((item) => (
                <div className="user-favorites-list">
                  <div className="favorites-card">
                    <UserProduct
                      id={item.product.data.id}
                      currency={item.product.data.currency}
                      title={item.product.data.title}
                      startPrice={item.product.data.start_price}
                      bidPrice={item.product.data.buy_now_price}
                      image={item.product.data.firstProductImage.data}
                      endDate={item.product.data.end_date}
                    />
                    <div className="user-product-edit pb-3">
                      <div className="product-icons">
                        <DeleteModal id={deleteItem} />
                      </div>
                      {item.bidPrice ? (
                        <div className="bid-now">
                          <button
                            type="button"
                            className="btn Sans_Bold addBid"
                          >
                            {intl.messages.bid_now}
                          </button>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Favorites;
