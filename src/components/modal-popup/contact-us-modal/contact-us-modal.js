import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { useIntl } from 'react-intl';


const ContactUsModal = () => {

    const [show, setShow] = useState(true);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const intl = useIntl();

    return (
        <>
        <Modal
          show={show}
          onHide={handleClose}
          animation={false}
          className="contact-modal"
        >
          <Modal.Header className=" delete-product" closeButton>
            <Modal.Title className="w-100 font-weight-bold text-center">
                {intl.messages.footer.footer_contact}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className='w-100 lot-color text-center font-24'>
          {intl.messages.contact_send}
          </Modal.Body>
        </Modal>
      </>   
    )
}

export default ContactUsModal