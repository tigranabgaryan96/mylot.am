import React, { useState } from 'react';
import './bid-now-modal.scss';
import { Modal } from 'react-bootstrap';
import { Divider } from 'antd';

import { bidRequest_req } from '../../../api/bid-request/bid.request.api';
import { useIntl } from 'react-intl';

const BidNowModal = ({
  props,
  price,
  show,
  handleClose,
  onClickBid,
  success,
}) => {
  const intl = useIntl();

  const data = Number(props.data.highest_suggestion) + Number(price);
  return (
    <React.Fragment>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header className="border-0 delete-product d-flex flex-column">
          <Modal.Title className="w-100 text-center font-24">
            {success?.status === 'Success' ? (
              <span>{success.message}</span>
            ) : (
              <span>{intl.messages.modal.bid_now_title}</span>
            )}
          </Modal.Title>
          <div className="w-100 d-flex justify-content-around mt-3 error-msg">
            {success.status === 'Error' ? (
              <span>{success.message}</span>
            ) : (
              <>
                <div>
                  <span className="font-14 Sans_Regular">
                    {intl.formatNumber(Number(data))}
                    <span className="ml-1"></span>
                  </span>
                </div>
                <div>
                  <span className="font-14 font-weight-bold">
                    <span className="ml-1"></span>
                    {props.data.currency}
                  </span>
                </div>
              </>
            )}
          </div>
        </Modal.Header>
        <Divider />
        <Modal.Footer className="border-0 d-flex justify-content-around">
          {success.status === 'Success' ? (
            <button className="btn buyCancel font-16" onClick={handleClose}>
              {intl.messages.close}
            </button>
          ) : (
            <>
              <button className="btn buyCancel font-16" onClick={handleClose}>
                {intl.messages.no}
              </button>
              <button
                className="btn buySucces font-16"
                onClick={() => onClickBid()}
              >
                {intl.messages.yes}
              </button>
            </>
          )}
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default BidNowModal;
