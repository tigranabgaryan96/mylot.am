import React, { useState } from 'react';
import './first-bid-modal.scss';
import '../modalpopup.scss';
import { Modal } from 'react-bootstrap';
import { useIntl } from 'react-intl';

import BigButton from '../../../app-components/big-button/big-button';
import { firstBid_req } from '../../../api/bid-request/bid.request.api';
import { formatNumberWithCharacter } from '../../../utils/formatter.util';
import { useDispatch, useSelector } from 'react-redux';
import { changeBid } from '../../../redux/first-bid.slice';

const FirstBid = ({ price, currency, productId }) => {
  const intl = useIntl();
  const [show, setShow] = useState(false);

  const dispatch = useDispatch();
  const [payment, setPayment] = useState('');
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const firstBidData = useSelector((state) => state.firstBid);
  const firstBid = () => {
    const bidFirstRequest = async () => {
      try {
        const bidRequestForm = await firstBid_req(productId, price, currency);
        dispatch(changeBid(true));
        setPayment(bidRequestForm);
      } catch (e) {}
    };
    bidFirstRequest();
  };

  return (
    <>
      <BigButton
        _onClick={() => {
          handleShow();
        }}
        text={intl.messages.lot.start_price}
      />
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header className="border-0 delete-product">
          <Modal.Title className="w-100 text-center font-20">
            {firstBidData.firstBid === false && (
              <p className="modal_title">{intl.messages.modal.start_price}</p>
            )}

            {payment.status === 'Error' ? <p>{payment.message}</p> : null}
            {payment.status === 'Success' ? payment.message : null}
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div className="d-flex justify-content-center modal_body lot-color">
            <span>{intl.messages.Pay_price} ՝</span>{' '}
            {formatNumberWithCharacter(price)} {currency}
          </div>
        </Modal.Body>
        <Modal.Footer className="border-0 d-flex justify-content-around">
          {firstBidData.firstBid ? (
            <button className="btn deleteCancel font-16" onClick={handleClose}>
              {intl.messages.no}
            </button>
          ) : (
            <>
              <button
                className="btn deleteCancel font-16"
                onClick={handleClose}
              >
                {intl.messages.no}
              </button>
              <button className="btn deleteDelete font-16" onClick={firstBid}>
                {intl.messages.yes}
              </button>
            </>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default FirstBid;
