import React, { useState } from 'react';
import '../modalpopup.scss';
import { Modal } from 'react-bootstrap';
import DELETE from '../../../assets/images/UserProduct/delete.svg';
import { deleteProduct_req } from '../../../api/product/product.api';
import { useIntl } from 'react-intl';

const DeleteModal = (props) => {
  const intl = useIntl();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const deleteProduct = async (id) => {
    try {
      const deleteProductReq = await deleteProduct_req(25);
    } catch (e) {}
  };

  return (
    <>
      <button onClick={handleShow} className="btn btn-icon">
        <img src={DELETE} alt="DELETE" />
      </button>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header>
          <Modal.Title className="w-100 text-center font-24">
            <span> {intl.messages.delete_text} </span>
          </Modal.Title>
          <Modal.Body></Modal.Body>
        </Modal.Header>
        <Modal.Footer className="border-0 dborder-0 delete-product-flex justify-content-around">
          <button className="btn deleteCancel font-16" onClick={handleClose}>
            {intl.messages.no}
          </button>
          <button
            className="btn deleteDelete font-16"
            onClick={() => deleteProduct(props.id)}
          >
            {intl.messages.yes}
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default DeleteModal;
