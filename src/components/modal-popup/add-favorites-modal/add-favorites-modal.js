import React, { useState } from 'react';
import '../modalpopup.scss';
import { Modal } from 'react-bootstrap';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';

const AddFavorites = () => {
  const [show, setShow] = useState(true);

  const handleClose = () => setShow(false);

  const intl = useIntl();

  let history = useHistory();

  function FavoritesClick() {
    history.push('/favoritesContainer');
  }

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        animation={false}
        className="favorites-modal"
      >
        <Modal.Header className="border-0 delete-product">
          <Modal.Title className="w-100 text-center font-24">
            {intl.messages.favorites.favorites_popup}
          </Modal.Title>
        </Modal.Header>
        <Modal.Footer className="border-0 d-flex justify-content-center">
          <button className="btn close-btnMd font-16" onClick={FavoritesClick}>
            {intl.messages.pay_success_btn}
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default AddFavorites;
