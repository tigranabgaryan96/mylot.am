import React, { useState } from 'react';
import './delete-favorites-product-modal.scss';
import '../modalpopup.scss';
import { Modal } from 'react-bootstrap';
import DELETE from '../../../assets/images/UserProduct/delete.svg';
import { deleteProduct_req } from '../../../api/product/product.api';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';

const DeleteFavoritesProductModal = (props) => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const [show, setShow] = useState(false);
  const [success, setSuccess] = useState('');

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const deleteProduct = async (id) => {
    try {
      const deleteProductReq = await deleteProduct_req(id);
      dispatch({ type: 'FAVORITES_REDUCER', deleteProductReq });
      setSuccess(deleteProductReq);
    } catch (e) {}
  };
  return (
    <>
      <button
        onClick={handleShow}
        className="btn btn-icon Delete_modal"
      >
        <img src={DELETE} alt="DELETE" />
      </button>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header className="border-0 delete-product">
          <Modal.Title className="w-100 text-center font-24">
            {!success ? (
              <span>{intl.messages.delete_favorites_text}</span>
            ) : null}
            <span>{success.status === 'Error' ? success.message : null}</span>
            <span>{success.status === 'Success' ? success.message : null}</span>
          </Modal.Title>
        </Modal.Header>
        <Modal.Footer className="border-0 d-flex">
          {!success ? (
            <div className="delete-favorites-modal-button">
              {' '}
              <button
                className="btn deleteCancel font-16"
                onClick={handleClose}
              >
                {intl.messages.no}
              </button>
              <button
                className="btn deleteDelete font-16"
                onClick={() => deleteProduct(props.id)}
              >
                {intl.messages.yes}
              </button>
            </div>
          ) : (
            <button
              className="btn font-16 button_close"
              onClick={handleClose}
            >
              {intl.messages.close}
            </button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default DeleteFavoritesProductModal;
