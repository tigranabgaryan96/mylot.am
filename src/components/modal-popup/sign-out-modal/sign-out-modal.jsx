import React, { useState } from 'react';
import './sign-out-modal.scss';
import { Modal } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { logOut_req } from '../../../api/sign-out/sign.out.api';
import { useIntl } from 'react-intl';

const SignOut = () => {
  const token = localStorage.token;
  const intl = useIntl();
  let history = useHistory();
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const dispatch = useDispatch();
  const logOut = async () => {
    try {
      const logOutUser = await logOut_req(token);
      dispatch({ type: 'LOG_OUT', logOutUser });
      history.push(`/home`);
    } catch (e) {}
  };
  return (
    <>
      <a onClick={handleShow} className="font-14 out-btn">
        {intl.messages.sign_out.button_out}
      </a>
      <Modal
        show={show}
        onHide={handleClose}
        animation={false}
        className="sign-out"
      >
        <Modal.Header>
          <Modal.Title className="w-100 text-center font-24">
            {intl.messages.sign_out.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="d-flex justify-content-around px-3">
            <button className="sign-out-button" onClick={handleClose}>
              <span>{intl.messages.sign_out.button_return}</span>
            </button>
            <button className="sign-out-button" onClick={logOut}>
              <span>{intl.messages.sign_out.button_out}</span>
            </button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default SignOut;
