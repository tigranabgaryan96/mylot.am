import React, { useEffect } from 'react';
import './help-center.scss';
import { useState } from 'react';

import { useIntl } from 'react-intl';
import {
  AfterVictory_req,
  aboutCommision_req,
  Foreword_req,
  HowToUse_req,
  Important_req,
  MessagesBenefits_req,
} from '../../api/help-center/help-center';
import { privacy_req } from '../../api/privacy/privacy.api';

const HelpCenter = () => {
  const [naxaban, setNaxaban] = useState(false);
  const [naxabanReq, setNaxabanReq] = useState('');
  const [help, setHelp] = useState(false);
  const [helpReq, setHelpReq] = useState('');
  const [win, setWin] = useState(false);
  const [winReq, setWinReq] = useState('');
  const [sms, setSms] = useState(false);
  const [smsReq, setSmsReq] = useState('');
  const [commision, setCommision] = useState(false);
  const [politics, setPolitics] = useState(false);
  const [commisionReq, setCommisionReq] = useState('');
  const [possible, setPossible] = useState(false);
  const [possibleReq, setPossibleReq] = useState('');
  const [privacyPolicy, setPrivacyPolicy] = useState('');
  const intl = useIntl();

  const naxabanRequest = async () => {
    try {
      const hakircText = await Foreword_req();
      setNaxabanReq(hakircText);
    } catch (e) {}
  };
  const helpRequest = async () => {
    try {
      const hakircText = await HowToUse_req();
      setHelpReq(hakircText);
    } catch (e) {}
  };
  const winRequest = async () => {
    try {
      const hakircText = await AfterVictory_req();
      setWinReq(hakircText);
    } catch (e) {}
  };

  const smsRequest = async () => {
    try {
      const hakircText = await MessagesBenefits_req();
      setSmsReq(hakircText);
    } catch (e) {}
  };
  const commisionRequest = async () => {
    try {
      const hakircText = await aboutCommision_req();
      setCommisionReq(hakircText);
    } catch (e) {}
  };
  const possibleRequest = async () => {
    try {
      const hakircText = await Important_req();
      setPossibleReq(hakircText);
    } catch (e) {}
  };
  const privacy = async () => {
    try {
      const authForm = await privacy_req();
      setPrivacyPolicy(authForm);
    } catch (e) {}
  };
  useEffect(() => {
    naxabanRequest();
    helpRequest();
    winRequest();
    smsRequest();
    commisionRequest();
    possibleRequest();
    privacy();
  }, []);

  const data = privacyPolicy?.data?.[0]?.text;

  const createMarkup = () => {
    return { __html: data };
  };

  const naxabanData =
    naxabanReq &&
    naxabanReq.data &&
    naxabanReq.data[0] &&
    naxabanReq.data[0].text;
  const helpData =
    helpReq && helpReq.data && helpReq.data[0] && helpReq.data[0].text;

  const winData =
    winReq && winReq.data && winReq.data[0] && winReq.data[0].text;

  const smsData =
    smsReq && smsReq.data && smsReq.data[0] && smsReq.data[0].text;

  const possibleData =
    possibleReq &&
    possibleReq.data &&
    possibleReq.data[0] &&
    possibleReq.data[0].text;

  const commisionData =
    commisionReq &&
    commisionReq.data &&
    commisionReq.data[0] &&
    commisionReq.data[0].text;

  const naxabanFn = () => {
    setNaxaban(!naxaban);
  };
  const helpFn = () => {
    setHelp(!help);
  };
  const winFn = () => {
    setWin(!win);
  };
  const smsFn = () => {
    setSms(!sms);
  };
  const commisionFn = () => {
    setCommision(!commision);
  };
  const possibleFn = () => {
    setPossible(!possible);
  };
  const possiblePolitics = () => {
    setPolitics(!politics);
  };

  const naxabanFunc = () => {
    return { __html: naxabanData };
  };
  const helpFunc = () => {
    return { __html: helpData };
  };
  const winFunc = () => {
    return { __html: winData };
  };
  const smsFunc = () => {
    return { __html: smsData };
  };
  const commisionFunc = () => {
    return { __html: commisionData };
  };
  const possibleFunc = () => {
    return { __html: possibleData };
  };
  return (
    <React.Fragment>
      <div className="main_grey_content">
        <div className="help-center">
          <div className="container">
            <div className="help-center_content">
              <h3 className="text-center">
                {intl.messages.help_center.help_title}
              </h3>
              <div className="help-center-list">
                <ul>
                  <div className="help-center-block">
                    <li onClick={naxabanFn} className="help-title">
                      1. {intl.messages.help_center.preface}
                    </li>
                    {naxaban ? (
                      <div
                        className="card card-body"
                        dangerouslySetInnerHTML={naxabanFunc()}
                      />
                    ) : null}
                  </div>
                  <div className="help-center-block">
                    <li onClick={helpFn} className="help-title">
                      2. {intl.messages.help_center.how_to_use}
                    </li>
                    {help ? (
                      <div
                        className="card card-body"
                        dangerouslySetInnerHTML={helpFunc()}
                      />
                    ) : null}
                  </div>
                  <div className="help-center-block">
                    <li onClick={winFn} className="help-title">
                      3. {intl.messages.help_center.after_the_victory}
                    </li>
                    {win ? (
                      <div
                        className="card card-body"
                        dangerouslySetInnerHTML={winFunc()}
                      />
                    ) : null}
                  </div>
                  <div className="help-center-block">
                    <li onClick={smsFn} className="help-title">
                      4. {intl.messages.help_center.messages_other_benefits}
                    </li>
                    {sms ? (
                      <div
                        className="card card-body"
                        dangerouslySetInnerHTML={smsFunc()}
                      />
                    ) : null}
                  </div>
                  <div className="help-center-block">
                    <li onClick={commisionFn} className="help-title">
                      5. {intl.messages.help_center.about_commission}
                    </li>
                    {commision ? (
                      <div
                        className="card card-body"
                        dangerouslySetInnerHTML={commisionFunc()}
                      />
                    ) : null}
                  </div>
                  <div className="help-center-block">
                    <li onClick={possibleFn} className="help-title">
                      6. {intl.messages.help_center.possible}
                    </li>
                    {possible ? (
                      <div
                        className="card card-body"
                        dangerouslySetInnerHTML={possibleFunc()}
                      />
                    ) : null}
                  </div>
                  <div className="help-center-block">
                    <li onClick={possiblePolitics} className="help-title">
                      7. {intl.messages.privacy_policy}
                    </li>

                    {politics ? (
                      <div
                        className="card card-body"
                        dangerouslySetInnerHTML={createMarkup()}
                      />
                    ) : null}
                  </div>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default HelpCenter;
