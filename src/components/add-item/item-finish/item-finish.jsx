import React, { useState, useEffect } from 'react';
import '../additem.scss';
import { Link, useHistory } from 'react-router-dom';
import { getProducts_req } from '../../../api/product/product.api';
import { Config } from '../../../constants/config';
import { useIntl } from 'react-intl';
import Preloader from '../../preloader/preloader';
import { useDispatch, useSelector } from 'react-redux';
import { finishData } from '../../../redux/add-product.slice';

const ItemFinish = ({ setCurrentTab }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [state, setState] = useState([]);

  const intl = useIntl();

  const data = useSelector((state) => state.addProduct.data.data);
  useEffect(() => {
    const getProduct = async () => {
      try {
        const getProductList = await getProducts_req(data.id);
        setState(getProductList);
      } catch (e) {}
    };
    getProduct();
  }, [data]);

  const goUserAuction = () => {
    dispatch(finishData(null));
    setCurrentTab(0);

    history.push('/my-auction');
  };
  const goAddItem = () => {
    dispatch(finishData(null));
    setCurrentTab(0);
    history.push('/addItem');
  };
  if (!state && !state.product && !state.product.data) {
    return <Preloader />;
  }

  return (
    <div className="row">
      <div className="col-12 col-lg-4">
        <div>
          <div>
            {state && state.product && state.product.data && (
              <img
                src={
                  Config.ImageUrl +
                  state.product.data.media.data[0].cover +
                  '_mediumOne.' +
                  state.product.data.media.data[0].ext
                }
                alt={state.product.data.media.data[0].cover}
                className="w-100 item_img"
              />
            )}
          </div>
        </div>
      </div>
      <div className="col-12 col-lg-8">
        <div className="addLot_finish">
          <h3>{intl.messages.add_item.your_lot_will}</h3>
          <div className="d-flex flex-column">
            {state && state.product && (
              <div>
                <p>
                  <div>{state.product.data.title}</div>
                </p>
                <p>
                  {intl.formatNumber(state.product.data.start_price)}
                  {intl.messages.money}
                </p>
              </div>
            )}
            <div className="mt-5">
              <div className="my-1">
                <Link className="btn finishBtn" onClick={goUserAuction}>
                  {intl.messages.my_lot}
                </Link>
              </div>
              <div className="my-1">
                <Link className="btn mt-2 finishBtn" onClick={goAddItem}>
                  {intl.messages.add_lot}
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemFinish;
