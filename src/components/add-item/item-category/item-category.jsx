import React, { useEffect } from 'react';
import './item-category.scss';
import { useIntl } from 'react-intl';
import Preloader from '../../preloader/preloader';
import { Menu, Divider } from 'antd';
import { useDispatch } from 'react-redux';
import { addProductCategory } from '../../../redux/add-product.slice';

const ItemCategory = ({ data, currentTab, setCurrentTab }) => {
  const dispatch = useDispatch();
  const intl = useIntl();
  useEffect(() => {
    dispatch(addProductCategory(''));
  }, []);
  const addCategory = (name) => {
    dispatch(addProductCategory(name));
    setCurrentTab(1);
  };
  if (!data || !data.data) {
    return <Preloader />;
  }

  const { SubMenu } = Menu;

  return (
    <React.Fragment>
      <div className="category_block_add">
        <h3 className="category-banner mt-3">
          {intl.messages.add_item.step_one_title}
        </h3>
        <Divider></Divider>
        <div className="category-item">
          {data.data.map((item) => (
            <Menu mode="inline">
              {item.child.data && item.child.data.length ? (
                <SubMenu key={item.id} title={item.name}>
                  {item.id &&
                    item?.child?.data?.map((child) => (
                      <Menu.ItemGroup>
                        {child.child.data && child.child.data.length ? (
                          <SubMenu key={child.id} title={child.name}>
                            {child.id &&
                              child.child &&
                              child.child.data &&
                              child.child.data.length &&
                              child.child.data.map((item) => (
                                <Menu.Item key={item.id}>
                                  <div onClick={() => addCategory(item.id)}>
                                    <span>{item.name}</span>
                                  </div>
                                  {/* <NavLink to={'addItem/compose/' + item.id}>
                                    {item.name}
                                  </NavLink> */}
                                </Menu.Item>
                              ))}
                          </SubMenu>
                        ) : (
                          <Menu.Item key={child.id}>
                            <div onClick={() => addCategory(child.id)}>
                              <span>{child.name}</span>
                            </div>
                            {/* <NavLink to={'addItem/compose/' + child.id}>
                              {child.name}
                            </NavLink> */}
                          </Menu.Item>
                        )}
                      </Menu.ItemGroup>
                    ))}
                </SubMenu>
              ) : (
                <Menu.Item key={item.id}>
                  <div onClick={() => addCategory(item.id)}>{item.name}</div>
                  {/* <NavLink to={'/addItem/compose/' + item.id}>
                    {item.name}
                  </NavLink> */}
                </Menu.Item>
              )}
            </Menu>
          ))}
        </div>
      </div>
    </React.Fragment>
  );
};

export default ItemCategory;
