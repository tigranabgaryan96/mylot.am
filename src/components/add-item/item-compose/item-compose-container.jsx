import React from 'react';
import AddProductItem from './item-compose';
import { getFilterGroup } from '../../../redux/FilterGroupReducer';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

class ItemComposeContainer extends React.Component {
  componentDidMount() {
    if (this.props) {
      const { categoryId } = this.props;

      this.props.getFilterGroup(categoryId);
    }
  }
  render() {
    return (
      <>
        {/* {!this.props.filterGroup ? <preloader /> : null} */}
        <AddProductItem {...this.props} props={this.props.filterGroup} />
      </>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    filterGroup: state.filterGroup.filterGroup,
    categoryId: state.addProduct.category,
  };
};

export default compose(
  connect(mapStateToProps, {
    getFilterGroup,
  }),
  withRouter
)(ItemComposeContainer);
