import React, { useState, useEffect } from 'react';
import './additem.scss';
import ItemFinish from './item-finish/item-finish';
import ItemCategory from './item-category/item-category';
import ItemComposeContainer from './item-compose/item-compose-container';
import { getCategory_req } from '../../api/category-list/category.list.api';
import Preloader from '../preloader/preloader';
import TabNavbar from './tab-navbar/tab-navbar';
import { useSelector } from 'react-redux';

const AddItem = () => {
  const [category, setCategory] = useState([]);
  const [currentTab, setCurrentTab] = useState(0);
  const categoryId = useSelector((state) => state.addProduct.category);
  const data = useSelector((state) => state?.addProduct?.data?.data);

  useEffect(() => {
    const categoryList = async () => {
      try {
        const categoryList = await getCategory_req();
        if (categoryList && categoryList.data) {
          setCategory(categoryList);
        }
      } catch (e) {}
    };
    categoryList();
  }, []);
  useEffect(() => {
    if (data) {
      setCurrentTab(2);
    }
  }, [data]);

  const tabContent = (currentTab) => {
    // eslint-disable-next-line default-case
    switch (currentTab + 1) {
      case 1:
        return (
          <ItemCategory
            data={category}
            currentTab={currentTab}
            setCurrentTab={setCurrentTab}
          />
        );

      case 2:
        if (categoryId) {
          return (
            <ItemComposeContainer
              currentTab={currentTab}
              setCurrentTab={setCurrentTab}
            />
          );
        } else {
          setCurrentTab(0);
        }

      // eslint-disable-next-line no-fallthrough
      case 3:
        if (data) {
          return <ItemFinish setCurrentTab={setCurrentTab} />;
        } else {
          setCurrentTab(0);
        }
    }
  };
  if (!category && category.length < 1) {
    return <Preloader />;
  }
  return (
    <div className="add-item py-5">
      <div className="container">
        <div className="card p-3">
          <div className="add-item-navbar">
            <TabNavbar currentTab={currentTab} setCurrentTab={setCurrentTab} />
          </div>
          <div className="add-item-content">{tabContent(currentTab)}</div>
        </div>
      </div>
    </div>
  );
};

export default AddItem;
