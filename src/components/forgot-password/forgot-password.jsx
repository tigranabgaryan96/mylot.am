import React, { useState } from 'react';
import './forgotpassword.scss';
import { forgotPassword_req } from '../../api/user-login/user.login.api';
import ButtonComponent from '../../app-components/button/button.components';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';

const ForgotPassword = () => {
  const [state, setState] = useState({ email: '' });
  const handleChange = (evt) => {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  };
  const intl = useIntl();
  let history = useHistory();
  const forgotPassword = () => {
    let email = state.email;
    history.push(window.open('https://mail.google.com/', '_blank'));
    const forgotPasswordReq = async () => {
      try {
        const userPassword = await forgotPassword_req(email);
      } catch (e) {}
    };
    forgotPasswordReq();
  };

  return (
    <div className="main_grey_content">
      <div className="forgot-password">
        <div className="container">
          <div className="row">
            <div className="forgot_content">
              <div className="card">
                <h4 className="text-center">
                  {intl.messages.registration.forgot_password}
                </h4>
                <p className="text-center">
                  {intl.messages.registration.forgot_password_title}
                </p>
                <div className="form-group">
                  <input
                    type="email"
                    name="email"
                    placeholder={intl.messages.registration.email}
                    value={state.email}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <div className="text-center py-3">
                  <ButtonComponent
                    _onClick={() => forgotPassword()}
                    text={intl.messages.registration.request_reset}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
