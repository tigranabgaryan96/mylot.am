import React, { useState } from 'react';
/*import './reset-password.scss';*/
import '../forgot-password/forgotpassword.scss';
import { resetPassword_req } from '../../api/user-login/user.login.api';
import { Redirect, useLocation } from 'react-router-dom';
import { useIntl } from 'react-intl';
import ButtonComponent from '../../app-components/button/button.components';

const ResetPassword = (props) => {
  const location = useLocation();
  const token = location.pathname.slice(26);
  const [state, setState] = useState({
    password: '',
    password_confirmation: '',
  });
  const [success, setSuccess] = useState(false);

  const intl = useIntl();
  const handleChange = (evt) => {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  };

  const resetPassword = () => {
    let password = state.password;
    let passwordConfirmation = state.password_confirmation;
    const resetPasswordReq = async () => {
      try {
        const userPassword = await resetPassword_req(
          token,
          password,
          passwordConfirmation
        );
        setSuccess(true);
      } catch (e) {}
    };
    resetPasswordReq();
  };

  return (
    <div className="main_grey_content">
      <div className="forgot-password">
        <div className="container">
          <div className="row">
            <div className="forgot_content">
              <div className="card">
                <h4>{intl.messages.registration.forgot_password}</h4>
                <p>{intl.messages.registration.forgot_password_title}</p>
                <div className="form-group mb-2">
                  <input
                    type="password"
                    name="password"
                    placeholder={intl.messages.registration.password}
                    value={state.password}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    name="password_confirmation"
                    placeholder={intl.messages.registration.confirme_password}
                    value={state.password_confirmation}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                {success ? <Redirect to="/home" /> : null}
                <div className="text-center py-3">
                  <ButtonComponent
                    _onClick={() => resetPassword()}
                    text={intl.messages.registration.request_reset}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ResetPassword;
