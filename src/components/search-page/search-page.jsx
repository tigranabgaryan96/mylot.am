import React from 'react';
import './search-page.scss';
import { NavLink } from 'react-router-dom';
import { useIntl } from 'react-intl';

const LiveImageUrl = 'https://mylot.am/storage/';

const SearchPage = (props) => {
  const intl = useIntl();
  const data = props.location.state.props.state.products;
  if (!data || !data.data) {
    return null;
  }
  return (
    <React.Fragment>
      <div className="container py-5">
        <nav className="nav py-4">
          <NavLink to="/home" className="text-dark mr-2">
            {intl.messages.main_page}
          </NavLink>
          <span>&gt;</span>
          <NavLink to="/search-page" className="active ml-2">
            {intl.messages.Research_page}
          </NavLink>
        </nav>
        <div className="p-3 card">
          <div className="itemsFound clearfix">
            <p className="foundItems">
              {intl.messages.Lot_lenght}
              <span className="ml-1">{data.data.length}</span>
            </p>
          </div>
          <div className="row">
            {data.data.map((products) => (
              <NavLink
                className="col-12 col-md-6 col-lg-4 col-xl-4 my-3 founded_item"
                to={'/product-page/' + products.id}
                key={products.id}
              >
                <div className="card containerCard h-100">
                  <div className="d-flex flex-column">
                    <img
                      src={
                        LiveImageUrl +
                        products.firstProductImage.data.cover +
                        '_mediumOne.' +
                        products.firstProductImage.data.ext
                      }
                      alt={products.firstProductImage.data.cover}
                      className="w-100 border-bottom productsImg"
                    />
                    <h4>{products.title}</h4>
                    <div className="p-3">
                      <div className="d-flex justify-content-between mt-2 founded_item_info">
                        <span>{intl.messages.product_info.lot_id_name}</span>
                        <span>{products.id}</span>
                      </div>
                      <div className="d-flex justify-content-between mt-2 founded_item_info">
                        <span>{intl.messages.lot.product_type}</span>
                        <span>{products.product_type}</span>
                      </div>
                      <div className="d-flex justify-content-between mt-2 founded_item_info">
                        <span>
                          {intl.messages.add_item.step_two_start_step}
                        </span>
                        <span>{intl.formatNumber(products.start_price)}</span>
                      </div>
                      {products.buy_now_price ? (
                        <div className="d-flex justify-content-between mt-2 founded_item_info">
                          <span>{intl.messages.lot.buy_now_price}</span>
                          <span>
                            {intl.formatNumber(products.buy_now_price)}
                          </span>
                        </div>
                      ) : null}
                      <div className="d-flex justify-content-between mt-2 founded_item_info">
                        <span>{intl.messages.product_info.end_date_text}</span>
                        <span>{products.end_date}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </NavLink>
            ))}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default SearchPage;
