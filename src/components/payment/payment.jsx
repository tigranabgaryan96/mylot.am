import React, { useEffect, useState } from 'react';
import './payment.scss';
import { paymentFinish_req } from '../../api/payment/payment.api';
import { useLocation, Link } from 'react-router-dom';
import { useIntl } from 'react-intl';

const Payment = () => {
  const [item, setItem] = useState('');
  const location = useLocation();
  const intl = useIntl();
  const payment = location.search;
  const paymentReq = payment.slice(6);

  useEffect(() => {
    const paymentFinish = async () => {
      try {
        const finishPayment = await paymentFinish_req(paymentReq);
        setItem(finishPayment);
      } catch (e) {}
    };
    paymentFinish();
  }, []);
  return (
    <div className="payment-done">
      {item.status === 'Success' && <span>{item.message}</span>}
      {item.status === 'Error' && <span>{item.message}</span>}
      <Link to={`/home`}> {intl.messages.payment.payment_link}</Link>
    </div>
  );
};
export default Payment;
