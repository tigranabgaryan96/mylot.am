import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { axiosAuthInstance } from '../../../api/auth.config';
import { favoritesProductList } from '../../../redux/favorites.slice';

const ProductFavoritesButton = ({ productId }) => {
  const [disableButton, setDisableButton] = useState(false);
  const [error, setError] = useState('');
  const dispatch = useDispatch();
  const handleClick = (productId) => {
    axiosAuthInstance
      .post(`/wishes?product_id=${productId}`)
      .then((response) => {
        if (response.data.status !== 'Error') {
          dispatch({ type: 'FAVORITES_ADD', payload: response.data });
          setDisableButton(!disableButton);
          return alert(
            ' Շնորհակալություն, այս լոտը ավելացվել է Ձեր նախընտրածների ցուցակում '
          );
        } else {
          setError(response.data);
        }
        return response.data;
      });
  };
  const intl = useIntl();
  return (
    <>
      {error && error.status === 'Error' ? error.message : null}
      <button
        className={disableButton ? 'add-default' : 'favorites-button'}
        onClick={() => handleClick(productId)}
      >
        <span>{intl.messages.favorites.favorites_subtitle}</span>
      </button>
    </>
  );
};

export default ProductFavoritesButton;
