import React, { useEffect, useState } from 'react';
import './product-page.scss';
import { Divider } from 'antd';

import { formatNumberWithCharacter } from '../../utils/formatter.util';
import { useIntl } from 'react-intl';
import './product-page.scss';
import Locetion from '../../assets/images/UserProduct/locetion.svg';
import CenterMode from './slick-slider/slick-slider';
import BuyNowModal from '../modal-popup/buy-now-modal/buy-now-modal';
import { commision_req } from '../../api/help-center/help-center';
import Preloader from '../preloader/preloader';
import { useSelector } from 'react-redux';
import FirstBid from '../modal-popup/first-bid-modal/first-bid-modal';
import BidNowModal from '../modal-popup/bid-now-modal/bid-now-modal';
import { useLocation } from 'react-router-dom';
import { getProductPage_req } from '../../api/api';
import { bidRequest_req } from '../../api/bid-request/bid.request.api';

const ProductPage = () => {
  const location = useLocation();
  const pathName = location.pathname;
  const productId = pathName.slice(14);

  const intl = useIntl();
  const [state, setState] = useState({
    price: '',
    buyNow: '',
  });
  const [itemData, setItemData] = useState(null);
  const [activeBtn, setActiveBtn] = useState(false);
  const [success, setSuccess] = useState('');
  const [show, setShow] = useState(false);
  const [commisionReq, setCommisionReq] = useState('');
  const [bidError, setBidError] = useState(null);

  const user = useSelector((state) => state.user);
  const firstBid = useSelector((state) => state.firstBid);
  const productData = async () => {
    const response = await getProductPage_req(productId);
    setItemData(response);
  };
  const commisionRequest = async () => {
    try {
      const hakircText = await commision_req();
      setCommisionReq(hakircText);
    } catch (e) {}
  };
  useEffect(() => {
    setTimeout(() => {
      productData();
    }, 1000);

    commisionRequest();
  }, []);

  useEffect(() => {
    if (state?.price > 0) {
      setActiveBtn(true);
    } else {
      setActiveBtn(false);
    }
  }, [state?.price]);
  const bidShow = () => {
    if (itemData.product.data.min_bid_price < state.price) {
      if (activeBtn) {
        setShow(true);
      }
    } else {
      setBidError('error');
    }
  };

  const onClickBid = async () => {
    let currency = itemData.product.data.currency;

    try {
      const bidRequestForm = await bidRequest_req(productId, price, currency);
      setSuccess(bidRequestForm);
      setState({
        price: '',
      });
    } catch (e) {}
  };
  const handleClose = () => {
    setShow(false);
  };
  const price =
    itemData &&
    itemData.product &&
    itemData.product.data &&
    itemData.product.data.start_price;
  const currency =
    itemData &&
    itemData.product &&
    itemData.product.data &&
    itemData.product.data.currency;

  if (!itemData?.product?.data) {
    return <Preloader />;
  }
  const handleChange = (evt) => {
    evt.preventDefault();
    // const value = Number(evt?.target?.value?.match(/\d+/g)?.join(''));
    const value = evt?.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  };

  const commisionData =
    commisionReq &&
    commisionReq.data &&
    commisionReq.data[0] &&
    commisionReq.data[0].text;
  const commisionTitle =
    commisionReq &&
    commisionReq.data &&
    commisionReq.data[0] &&
    commisionReq.data[0].title;
  const commisionFunc = () => {
    return { __html: commisionData };
  };
  const userData = user?.data?.id !== itemData.product.data.created_user_id;

  return (
    <React.Fragment>
      <div className="productlive-container py-5 product_container">
        <div className="container">
          <div className="p-4 card">
            <div className="row">
              <div className="col-12 col-lg-6">
                {itemData.product.data.media ? (
                  <div
                    className="
                    product-slider w-100"
                  >
                    <CenterMode
                      imgData={itemData.product.data.media}
                      className="w-100"
                    />
                  </div>
                ) : (
                  <Preloader />
                )}
              </div>
              <div className="col-12 col-lg-6">
                <div className="product_info">
                  <h3>{itemData.product.data.title}</h3>
                  <div className="d-flex">
                    <img src={Locetion} alt="LOCATION" />
                    <span className="ml-3">
                      {itemData.product.data.city.data.name}
                    </span>
                  </div>
                  <div className="mt-3">
                    {!user.data ||
                    itemData.product.data.live === false ? null : (
                      <div className="d-flex justify-content-between">
                        {itemData.product.data.filters.data ? (
                          <div className="d-flex justify-content-between w-100">
                            <span className="font-weight-bold font-14 Sans_Bold my-1">
                              {intl.messages.product_info.start_date_text} :
                            </span>
                            <span className="ml-3 font-14 time-danger my-1 font-weight-bold">
                              {itemData.product.data.start_date}
                            </span>
                          </div>
                        ) : null}
                      </div>
                    )}
                    {!user.data ||
                    itemData.product.data.live === false ? null : (
                      <div className="d-flex justify-content-between">
                        {itemData.product.data.filters.data ? (
                          <div className="d-flex justify-content-between w-100">
                            <span className="font-weight-bold font-14 Sans_Bold my-1">
                              {intl.messages.product_info.end_date_text} :
                            </span>
                            <span className="ml-3 font-14 time-danger my-1 font-weight-bold">
                              {itemData.product.data.end_date}
                            </span>
                          </div>
                        ) : null}
                      </div>
                    )}
                    <div className="d-flex flex-column my-2">
                      {itemData.product?.data?.filters?.data.map((item) => (
                        <div className="d-flex justify-content-between mt-3 product_list_block">
                          <div>
                            <span>{item.filterGroups.data.name}</span>
                          </div>
                          <div>
                            <span>
                              {item.value} {item.unity}
                            </span>
                          </div>
                        </div>
                      ))}
                      <div className="d-flex justify-content-between mt-3 product_list_block">
                        <div>
                          <span>
                            {intl.messages.favorites.number_of_favorites}
                          </span>
                        </div>
                        <div>
                          <span>{itemData.product.data.wishes_count}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Divider />
                  {/*  */}
                  <div className="d-flex flex-column my-2">
                    <span>{intl.messages.product_info.comment}</span>
                    <p className="product_description">
                      {itemData.product.data.description}
                    </p>
                  </div>
                  {/*  */}
                  <Divider />
                  <div>
                    <div className="d-flex justify-content-between mt-3 product_list_block">
                      <div>
                        <span>{intl.messages.lot.lot_id}</span>
                      </div>
                      <div>
                        <span>{itemData.product.data.id}</span>
                      </div>
                    </div>
                    <div className="d-flex justify-content-between mt-3 product_list_block">
                      <div>
                        <span>{intl.messages.lot.start_price}</span>
                      </div>
                      <div>
                        <span>
                          {intl.formatNumber(itemData.product.data.start_price)}
                          <span className="ml-1"></span>
                          {itemData.product.data.currency}
                        </span>
                      </div>
                    </div>
                    {itemData.product.data.highest_suggestion ? (
                      <div className="d-flex justify-content-between mt-3 product_list_block">
                        <div>
                          <span>{intl.messages.product_info.this_step}</span>
                        </div>
                        <div>
                          {success ? (
                            <span>
                              {intl.formatNumber(
                                Number(
                                  itemData.product.data.highest_suggestion +
                                    Number(price)
                                )
                              )}
                              <span className="ml-1"></span>
                              {itemData.product.data.currency}
                            </span>
                          ) : (
                            <span>
                              {intl.formatNumber(
                                itemData.product.data.highest_suggestion
                              )}
                              <span className="ml-1"></span>
                              {itemData.product.data.currency}
                            </span>
                          )}
                        </div>
                      </div>
                    ) : null}
                    {itemData.product.data.buy_now_price &&
                    !itemData.product.data.live ? (
                      <div className="d-flex justify-content-between mt-3 product_list_block">
                        <div>
                          <span>{intl.messages.lot.buy_now_price}</span>
                        </div>
                        <div>
                          <span>
                            {intl.formatNumber(
                              itemData.product.data.buy_now_price
                            )}
                            <span className="ml-1"></span>
                            {itemData.product.data.currency}
                          </span>
                        </div>
                      </div>
                    ) : null}
                    <div className="d-flex justify-content-between mt-3 product_list_block">
                      <div>
                        <span>
                          {intl.messages.add_item.step_two_minimum_step}
                        </span>
                      </div>
                      <div>
                        <span>
                          {intl.formatNumber(
                            itemData.product.data.min_bid_price
                          )}
                          <span className="ml-1"></span>
                          {itemData.product.data.currency}
                        </span>
                      </div>
                    </div>
                    {!user.data || itemData.product.data.live === false ? (
                      <div>
                        <div className="my-2">
                          {!user.data ? (
                            <span>
                              {intl.messages.product_info.created_user_id}
                            </span>
                          ) : null}
                        </div>
                      </div>
                    ) : (
                      <div></div>
                    )}
                    {/*  */}
                    {(user.data &&
                      itemData.product.data.live === true &&
                      userData &&
                      itemData.product.data.highest_suggestion) ||
                    firstBid.firstBid ? (
                      <div>
                        <div className="d-flex justify-content-between align-items-center mt-3">
                          <span className="your_move_text">
                            {intl.messages.product_info.your_move}
                          </span>
                          <span className="ml-3">
                            <div className="form-group mr-0 mb-0">
                              <input
                                name="price"
                                min={itemData.product.data.min_bid_price}
                                placeholder={intl.formatNumber(
                                  itemData.product.data.min_bid_price
                                )}
                                value={state.price}
                                onChange={handleChange}
                                className="form-control"
                                onKeyPress={(event) => {
                                  if (!/[0-9]/.test(event.key)) {
                                    event.preventDefault();
                                  }
                                }}
                              />
                            </div>
                            {bidError && <span>{bidError}</span>}
                          </span>
                        </div>
                      </div>
                    ) : null}
                    <div className="d-flex justify-content-end mt-5">
                      {(itemData.product.data.live === true &&
                        itemData.product.data.highest_suggestion &&
                        user?.data?.id !==
                          itemData.product.data.created_user_id) ||
                      firstBid.firstBid ? (
                        <button
                          className={`${
                            activeBtn ? ' active-bid-button' : 'bid-button '
                          }`}
                          onClick={bidShow}
                        >
                          <span>{intl.messages.product_info.take_step}</span>
                        </button>
                      ) : null}
                      {user?.data &&
                      itemData.product.data.live === true &&
                      !itemData.product.data.highest_suggestion &&
                      user?.data?.id !==
                        itemData.product.data.created_user_id ? (
                        <FirstBid
                          price={price}
                          currency={currency}
                          productId={productId}
                        />
                      ) : null}
                      {user.data || itemData.product.data.buy_now_price ? (
                        <>
                          {itemData.product.data.live === false &&
                          user &&
                          user.data &&
                          user.data.id !==
                            itemData.product.data.created_user_id &&
                          itemData.product.data.buy_now_price ? (
                            <BuyNowModal props={itemData.product} />
                          ) : null}
                        </>
                      ) : null}
                      {/*  */}
                    </div>
                  </div>
                  <Divider />
                </div>
              </div>
            </div>
          </div>
          <div className="mt-3 p-4 card product_more_info">
            <h3>{commisionTitle}</h3>
            <div className="row">
              <div className="col-12 col-md-12">
                <p dangerouslySetInnerHTML={commisionFunc()} />
              </div>
            </div>
          </div>
        </div>
      </div>
      {show && (
        <BidNowModal
          props={itemData.product}
          price={state.price}
          show={show}
          handleClose={handleClose}
          onClickBid={onClickBid}
          success={success}
        />
      )}
    </React.Fragment>
  );
};

export default ProductPage;
