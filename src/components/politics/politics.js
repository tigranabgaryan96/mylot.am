import React, { useState, useEffect } from 'react';

import { privacy_req } from '../../api/privacy/privacy.api';
import './politics.scss';
import { useIntl } from 'react-intl';

const Politics = () => {
  const [privacyPolicy, setPrivacyPolicy] = useState('');
  const intl = useIntl();
  const privacy = async () => {
    try {
      const authForm = await privacy_req();
      setPrivacyPolicy(authForm);
    } catch (e) {}
  };
  useEffect(() => {
    privacy();
  }, []);
  const data = privacyPolicy?.data?.[0]?.text;
  const createMarkup = () => {
    return { __html: data };
  };
  return (
    <div className="main_grey_content">
      <div className="politics">
        <div className="container">
          <div className="politics_content">
            <div className="text-center my-3">
              <h3>{intl.messages.privacy_policy}</h3>
            </div>
            <span dangerouslySetInnerHTML={createMarkup()} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Politics;
