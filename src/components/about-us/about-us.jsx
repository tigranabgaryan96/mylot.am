import React from 'react';
import './about-us.scss';

import facebook from '../../assets/images/Footer/facebook.svg';
import instagram from '../../assets/images/Footer/instagram.svg';
import google from '../../assets/images/Footer/google@2x.png';
import { useIntl } from 'react-intl';

const AboutUs = () => {
  //   useEffect(() => {
  //     const aboutUs = async () => {
  //       try {
  //         const aboutUsReq = await aboutUs_req();
  //       } catch (e) {}
  //     };
  //     aboutUs();
  //   }, []);

  const intl = useIntl();

  return (
    <div className="main_grey_content">
      <div className="about-container">
        <div className="container about-info">
          <h3 className="text-center">{intl.messages.about_us}</h3>
          <div className="text-justify">
            <p>{intl.messages.Hello}</p>
            <p>{intl.messages.about_us_page.title_one}</p>
            <p>{intl.messages.about_us_page.title_two}</p>
            <p>{intl.messages.about_us_page.title_three}</p>
          </div>
          <div className="about-us-soc">
            <ul className="leftBlock_contact">
              <li className="mx-2">
                <a
                  href={`https://www.facebook.com/Mylot_-108037730954035/, '_blank '`}
                >
                  <img src={facebook} alt="FACEBOOK" />
                </a>
              </li>
              <li className="mx-2">
                <a
                  href={`https://www.facebook.com/Mylot_-108037730954035/, '_blank'`}
                >
                  <img src={google} alt="GOOGLE" />
                </a>
              </li>
              <li className="mx-2">
                <a
                  href={`https://www.instagram.com/mylot2020/?hl=ru&fbclid=IwAR0_ahPL7Ku-Q_KRt1kW_paKbP6tDGhVH0yFr8oh2ZdzgXwfgTc5iZojCHI, '_blank`}
                >
                  <img src={instagram} alt="INSTAGRAM" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;
