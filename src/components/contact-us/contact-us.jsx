import React, { useState } from 'react';
import './contact-us.scss';
import ButtonComponent from '../../app-components/button/button.components';
import { useIntl } from 'react-intl';
import { contactUs_req } from '../../api/contact-us/contact-us';
import ContactUsModal from '../modal-popup/contact-us-modal/contact-us-modal';

const ContactUs = () => {
  const [state, setState] = useState({
    email: '',
    subject: '',
    message: '',
  });
  const [success, setSuccess] = useState('');

  const intl = useIntl();
  const contactUs = async () => {
    let email = state.email;
    let subject = state.subject;
    let message = state.message;
    try {
      const contactUsReq = await contactUs_req(email, subject, message);
      setSuccess(contactUsReq);
      setState({
        email: '',
        subject: '',
        message: '',
      });
    } catch (e) {}
  };
  const handleChange = (evt) => {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
  };

  return (
    <div className="main_grey_content">
      <div className="contactus-container">
        <div className="container">
          <div className="row">
            <div className="contact-info">
              <h3 className="text-center contact-title">
                {intl.messages.footer.footer_contact}
              </h3>
              <p className="text-center contact-subtitle">
                {intl.messages.contact_sending}
              </p>
              <div className="contact-block">
                <div className="contact-input d-flex flex-column flex-md-row">
                  <label>{intl.messages.registration.email}</label>
                  <input
                    type="text"
                    name="email"
                    placeholder={intl.messages.registration.email}
                    value={state.email}
                    onChange={handleChange}
                  />
                </div>
                <div className="contact-input d-flex flex-column flex-md-row">
                  <label>{intl.messages.contact_title}</label>
                  <input
                    type="text"
                    name="subject"
                    placeholder={intl.messages.contact_title}
                    value={state.subject}
                    onChange={handleChange}
                  />
                </div>
                <div className="contact-input d-flex flex-column flex-md-row">
                  <label>{intl.messages.sms}</label>
                  <textarea
                    type="text"
                    name="message"
                    placeholder={intl.messages.registration.name}
                    value={state.message}
                    onChange={handleChange}
                  />
                </div>
                <ButtonComponent
                  text={intl.messages.Desired_product_button}
                  _onClick={() => contactUs()}
                  className="px-5 py-1"
                />
              </div>
            </div>
            <div className="d-flex justify-content-center">
              {success.status === 'Success' ? <ContactUsModal /> : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactUs;
