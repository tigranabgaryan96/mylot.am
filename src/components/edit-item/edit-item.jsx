import React, { useState, useEffect } from 'react';
import './edit-item.scss';
import '../add-item/item-compose/item-compose.scss';
import { useParams, useHistory } from 'react-router-dom';
import { addModalProduct_req } from '../../api/add-product/add.product.api';
import {
  getCity_req,
  getRegions_req,
  getProducts_req,
} from '../../api/product/product.api';
import { updateProduct_req } from '../../api/update-product/update.product.api';
import DatePicker from 'react-datepicker';
import { useIntl } from 'react-intl';
import 'react-datepicker/dist/react-datepicker.css';
import { picutureUrlMapper } from '../../utils/imageData.util';
import UploadImage from './edit-images';
import ButtonComponent from '../../app-components/button/button.components';
import Upload from '../../assets/images/UserProduct/upload.png';

const EditItem = (props, { data }) => {
  const { id: productId } = useParams();
  let history = useHistory();
  const intl = useIntl();
  const [error, setError] = useState(false);
  const [startDate, setStartDate] = useState(
    new Date(Date.now() + 3600 * 1000 * 24)
  );
  const [endDate, setEndDate] = useState(null);
  const [product, setProduct] = useState([]);
  const [state, setState] = useState({
    title: '',
    description: '',
    start_price: '',
    min_bid_price: '',
    region_id: '',
    city_id: '',
    auction_type: '',
    product_type: '',
    currency: '',
    images: '',
    start_date: '',
    end_date: '',
  });

  const [upRegions, setRegions] = useState([]);
  const [upCity, setCity] = useState([]);
  const [parentFilter, setParentFilter] = useState({});
  const upCityProducts = async () => {
    const cityProductsList = await getCity_req();
    setCity(cityProductsList);
  };
  const upRegionProducts = async () => {
    const regionProductsList = await getRegions_req();
    setRegions(regionProductsList);
  };
  const endDateChange = (date) => {
    setEndDate({
      endDate: date,
    });
  };
  console.log('endDate', endDate);
  useEffect(() => {
    setState({
      ...state,
      end_date: endDate ? endDate?.endDate : product?.product?.data.end_date,
    });
  }, [endDate]);
  const dateChange = (date) => {
    console.log('datar', date);
    setStartDate({
      startDate: date,
    });
  };
  // useEffect(() => {
  //   setState({
  //     ...state,
  //     start_date: startDate?.startDate,
  //   });
  // }, [startDate]);

  const getProduct = async () => {
    try {
      const productRes = await getProducts_req(productId);

      const {
        product: {
          data: {
            filters: { data: filters },
            description,
            city: {
              data: { id: city_id, region_id },
            },
            title,
            start_price,
            min_bid_price,
            auction_type,
            currency,
            category: {
              data: { id: category },
            },
            media: { data: media },
          },
        },
      } = productRes;
      const defaultState = {
        ...state,
        description,
        city_id,
        region_id,
        title,
        start_price,
        min_bid_price,
        auction_type,
        currency,
        category,
        images: media.map((item) => picutureUrlMapper(item)),
      };

      const tempState = filters.reduce(
        (
          st,
          {
            id,
            filterGroups: {
              data: { key },
            },
          }
        ) => {
          st[key] = id;
          return st;
        },
        defaultState
      );
      setState(tempState);
      // setState({
      //   ...state,
      //   end_date: productRes?.product?.data.end_date,
      // });
      setProduct(productRes);
    } catch (e) {
      console.error(e);
    }
  };
  console.log('state', state.end_date);
  useEffect(() => {
    getProduct();
    upRegionProducts();
    upCityProducts();
  }, []);

  const handleChange = (event, item) => {
    const {
      target: { name, value },
    } = event;
    setState({
      ...state,
      [name]: value,
    });
    if (item) {
      addModalProduct_req(value).then(({ data }) => {
        const { filter_group_id } = item;
        setParentFilter({
          ...parentFilter,
          [filter_group_id]: data,
        });
      });
    }
  };

  const addProduct = async () => {
    try {
      const addProductList = await updateProduct_req(
        productId,
        state,
        startDate
      );
      if (addProductList) {
        history.push(`product-page/${addProductList.data.id}`);
      }
    } catch (e) {
      console.error(e);
      setError(e.response);
    }
  };

  return (
    <React.Fragment>
      <div className="container py-5">
        <div className="card p-4">
          {/* Edit Images */}
          <UploadImage
            data={state.images}
            getState={(images) =>
              setState({
                ...state,
                images,
              })
            }
            uploadTitle={intl.messages.download_imgaes}
            uploadImage={Upload}
            withIcon={false}
            withLabel={false}
            className="col-12 col-sm-6 col-md-4 col-xl-3 flex-column"
          >
            {(images) => (
              <div className="upload__image-wrapper">
                <div className="d-flex flex-wrap">
                  {images?.map((img) => (
                    <div>
                      <img
                        key={img.cover}
                        src={img.cover}
                        alt={img.cover}
                        className="imageItem m-2"
                      />
                    </div>
                  ))}
                </div>
              </div>
            )}
          </UploadImage>
          {/* Edit Images  */}
          <div className="edit_item_block">
            <div className="d-flex flex-column">
              {product?.product?.data?.filters?.data ? (
                <div className="group-container">
                  {product.product.data.filters.data.map((item) => {
                    const {
                      id,
                      filterGroups: {
                        data: { type, name, key, filters, parent_id },
                      },
                    } = item;

                    return (
                      <div key={id}>
                        {type === 'select' ? (
                          <div className="form-group">
                            <label className="col-12 col-md-4 col-lg-3">
                              {name}
                            </label>
                            <select
                              value={state[key]}
                              onChange={(event) => handleChange(event, item)}
                              name={key}
                              className="composer-selecter col-12 col-md-8 col-lg-5 mt-3 mt-md-0"
                            >
                              <option selected>
                                {intl.messages.add_item.step_one_title} {name}
                              </option>
                              {parentFilter[parent_id]
                                ? parentFilter[parent_id].map(
                                    ({ id: itemId, value: itemValue }) => (
                                      <option
                                        key={itemId}
                                        value={itemId}
                                        className="my-1 item-product"
                                      >
                                        {itemValue}
                                      </option>
                                    )
                                  )
                                : filters?.data.map(
                                    ({ id: itemId, value: itemValue }) => (
                                      <option
                                        key={itemId}
                                        value={itemId}
                                        className="item-product"
                                      >
                                        {itemValue}
                                      </option>
                                    )
                                  )}
                            </select>
                            {error ? error.data.errors[key] : null}
                          </div>
                        ) : (
                          <div>
                            {item &&
                            item.filterGroup &&
                            item.filterGroup &&
                            item.filterGroup.data ? (
                              <div className="form-group d-md-flex">
                                <label className="col-12 col-md-4 col-lg-3">
                                  {item.filterGroup.data.name}
                                </label>
                                <div className="col-12 d-flex pl-0 pr-0 col-md-8 col-lg-3 mt-3 mt-md-0">
                                  <input
                                    type="text"
                                    placeholder={item.filterGroup.data.name}
                                    name={key}
                                    value={state[key]}
                                    onChange={handleChange}
                                    className={'form-control '}
                                  />
                                  {item &&
                                    item.filterGroup &&
                                    item.filterGroup.data &&
                                    item.filterGroup.data.unity && (
                                      <select
                                        name={key + '_unity'}
                                        onChange={handleChange}
                                        value={state[key + '_unity']}
                                      >
                                        <option>
                                          {
                                            intl.messages.add_item
                                              .step_one_title
                                          }
                                        </option>
                                        {item.filterGroup.data.unity.map(
                                          (item) => (
                                            <option>{item}</option>
                                          )
                                        )}
                                      </select>
                                    )}
                                </div>
                              </div>
                            ) : null}
                            {error ? error.data.errors[key] : null}
                          </div>
                        )}
                      </div>
                    );
                  })}
                </div>
              ) : null}
            </div>
            {/* Start Date */}
            <div className="form-group">
              <div className="group-container d-block d-md-flex">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.lot.start_date}
                </label>
                <DatePicker
                  name="start_date"
                  selected={state.start_date}
                  onChange={dateChange}
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={30}
                  timeCaption="time"
                  dateFormat="MMMM d, yyyy h:mm aa"
                  className="col-12 col-md-8 col-lg-4 mt-3 mt-md-0"
                  type="data"
                  selected={startDate}
                />
              </div>
            </div>
            {/* End Date */}
            <div className="form-group">
              <div className="group-conatiner d-block d-md-flex">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.lot.end_date}
                </label>
                <DatePicker
                  name="end_date"
                  selected={state.end_date}
                  onChange={endDateChange}
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={30}
                  timeCaption="time"
                  dateFormat="MMMM d, yyyy h:mm aa"
                  className="col-12 col-md-8 col-lg-4 mt-3 mt-md-0"
                  type="data"
                  selected={state.endDate}
                />
              </div>
            </div>
            {/* Region */}
            <div className="form-group">
              <div className="group-conatiner">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.add_item.step_two_country_state}
                </label>
                <select
                  value={state.region_id}
                  onChange={handleChange}
                  name="region_id"
                  className="composer-selecter col-12 col-md-8 col-lg-3 mt-3 mt-md-0"
                >
                  <option>
                    {intl.messages.add_item.step_two_country_state}
                  </option>
                  {upRegions &&
                    upRegions.data &&
                    upRegions.data.map((item) => (
                      <option key={item.id} value={item.id}>
                        {item.id}.{item.name}
                      </option>
                    ))}
                </select>
                {error ? error.data.errors.region_id : null}
              </div>
            </div>
            {/* City */}
            <div className="form-group">
              <div className="group-conatiner">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.add_item.step_two_city_name}
                </label>
                <select
                  value={state.city_id}
                  onChange={handleChange}
                  name="city_id"
                  required
                  className="composer-selecter col-12 col-md-8 col-lg-3 mt-3 mt-md-0"
                >
                  <option>{intl.messages.add_item.step_two_city_name}</option>
                  {upCity &&
                    upCity.data &&
                    upCity.data.map((item) => (
                      <option
                        key={item.id}
                        value={item.id}
                        className="item-product"
                      >
                        {item.id}.{item.name}
                      </option>
                    ))}
                </select>
                {error ? error.data.errors.city_id : null}
              </div>
            </div>
            {/* Auction Type */}
            <div className="form-group">
              <div className="group-conatiner">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.lot.product_type}
                </label>
                <select
                  onChange={handleChange}
                  name="auction_type"
                  value={state.auction_type}
                  required
                  className="composer-selecter col-12 col-md-8 col-lg-3 mt-3 mt-md-0"
                >
                  <option>{intl.messages.lot.product_type}</option>
                  <option value="standard">
                    {intl.messages.add_item.step_two_currency_standart}
                  </option>
                  <option value="holland">
                    {intl.messages.add_item.step_two_currency_holande}
                  </option>
                </select>
                {error ? error.data.errors.auction_type : null}
              </div>
            </div>
            {/* Currency */}
            <div className="form-group">
              <div className="group-conatiner">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.add_item.step_two_currency}
                </label>
                <select
                  onChange={handleChange}
                  name="currency"
                  value={state.currency}
                  required
                  className="composer-selecter col-12 col-md-8 col-lg-3 mt-3 mt-md-0"
                >
                  <option>{intl.messages.add_item.step_two_currency}</option>
                  <option value="AMD">AMD</option>
                  <option value="RUB">RUB</option>
                </select>
                {error ? error.data.errors.auction_type : null}
              </div>
            </div>
            {/* Start Price */}
            <div className="form-group">
              <div className="group-conatiner d-md-flex">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.add_item.step_two_start_step}
                </label>
                <input
                  type="text"
                  name="start_price"
                  placeholder={intl.messages.add_item.step_two_start_step}
                  value={state.start_price}
                  required
                  onChange={handleChange}
                  className="form-control col-12 col-md-8 col-lg-3 mt-3 mt-md-0"
                />
              </div>
              {error ? error.data.errors.start_price : null}
            </div>
            {/* Min Bid Proce */}
            <div className="form-group">
              <div className="group-conatiner d-md-flex">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.add_item.step_two_minimum_step}
                </label>
                <input
                  type="text"
                  name="min_bid_price"
                  placeholder={intl.messages.add_item.step_two_minimum_step}
                  value={state.min_bid_price}
                  required
                  onChange={handleChange}
                  className="form-control col-12 col-md-8 col-lg-3 mt-3 mt-md-0"
                />
              </div>
              {error ? error.data.errors.min_bid_price : null}
            </div>
            {/* Title Product */}
            <div className="form-group">
              <div className="group-conatiner d-md-flex">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.registration.name}
                </label>
                <input
                  type="text"
                  placeholder={intl.messages.registration.name}
                  name="title"
                  required
                  value={state.title}
                  defaultValue={
                    product && product.product
                      ? product.product.data.title
                      : null
                  }
                  onChange={handleChange}
                  className="form-control col-12 col-md-8 col-lg-9 mt-3 mt-md-0"
                />
                {error ? error.data.errors.title : null}
              </div>
            </div>
            {/* Description Product */}
            <div className="form-group">
              <div className="group-conatiner d-md-flex">
                <label className="col-12 col-md-4 col-lg-3">
                  {intl.messages.add_item.step_two_textarea}
                </label>
                <textarea
                  type="textarea"
                  name="description"
                  placeholder={intl.messages.add_item.step_two_textarea}
                  value={state.description}
                  onChange={handleChange}
                  required
                  className="form-control controler-doscription col-12 col-md-8 col-lg-9 mt-3 mt-md-0"
                ></textarea>
              </div>
              {error ? error.data.errors.description : null}
            </div>
            {/* Add Product */}
            <div className="d-flex justify-content-end mt-3">
              <ButtonComponent
                _onClick={() => addProduct()}
                text={intl.messages.add_item.step_two_btn}
                className="add-product-btn"
              />
            </div>
            <div className="text-left font-12"></div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default EditItem;
