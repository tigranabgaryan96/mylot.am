import React from 'react';
import './suggest-item.scss';
import { useIntl } from 'react-intl';

const SuggestItem = (suggest) => {
  const intl = useIntl();

  return (
    <div className="p-2 suggest_items">
      <h3 className="suggest_items_text">{intl.messages.Desired_product}</h3>
    </div>
  );
};

export default SuggestItem;
