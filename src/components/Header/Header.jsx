import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './header.scss';
import HeaderRight from './header-right/header-right';
import { Navbar, Nav } from 'react-bootstrap';
import Search from './search/search';
import CategoryList from './category-list/category-list';
import { useIntl } from 'react-intl';

import UserIcon from './user/user';
import Registers from './register/register';
import { changeMenu } from '../../redux/menu.slice';
import { useHistory, useLocation } from 'react-router-dom';
import Popup from '../popup/popup';

const Header = () => {
  const intl = useIntl();
  const location = useLocation();

  const history = useHistory();
  const dispatch = useDispatch();

  const [drop, setDrop] = useState(false);
  const [open, setOpen] = useState(false);
  const [openPopup, setOpenPopup] = useState(false);
  const node = useRef();
  const user = useSelector((state) => state.user);

  const locationData = location.pathname.slice(1);

  const useOnClickOutside = (ref, handler, setDrop) => {
    useEffect(() => {
      const listener = (event) => {
        if (!ref.current || ref.current.contains(event.target)) {
          return;
        }
        setDrop(false);

        handler(event);
      };
      document.addEventListener('mousedown', listener);
      return () => {
        document.removeEventListener('mousedown', listener);
      };
    }, [ref, handler, setDrop]);
  };
  useOnClickOutside(
    node,
    () => setOpen(false),
    () => setDrop(false)
  );
  const closeMenu = () => {
    setOpen(!open);
  };

  const closePopup = () => {
    setOpenPopup(!openPopup);
  };

  const goHome = () => {
    setOpenPopup(!openPopup);
    history.push('/');
  };

  const goHomePage = () => {
    if (locationData === 'addItem') {
      setOpenPopup(!openPopup);
    } else {
      history.push('/');
    }
  };

  return (
    <>
      {' '}
      <div className="header">
        <Navbar className="justify-content-between navbar-custom navbar">
          <Nav className="w-100 nav menu_navbar">
            <div className="menu">
              <div ref={node} className="menu-button">
                <div className="menu-button" open={open} onClick={closeMenu}>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
                <CategoryList
                  open={open}
                  drop={drop}
                  setDrop={setDrop}
                  closeMenu={closeMenu}
                />
              </div>
              <div className="menu-text d-none d-md-block">
                <p>{intl.messages.menu}</p>
              </div>
            </div>
            <Search />
          </Nav>
          <div className="logo" onClick={goHomePage}>
            <img
              src={require('../../assets/images/Header/mylot_new.png')}
              alt="MYLOT"
              className="logoImg"
            />
          </div>
          <Nav className="w-100 justify-content-end users-right nav">
            {user.data ? <UserIcon /> : <Registers />}
            <HeaderRight token={user.data} />
          </Nav>
        </Navbar>
      </div>
      {openPopup && (
        <Popup trigger={openPopup}>
          <div className="content">
            <p>Դուք ցանկանում եք գնալ գլխավոր էջ?</p>
            <div className="btn-block">
              <div className="btn-item" onClick={closePopup}>
                <span>{intl.messages.no}</span>
              </div>
              <div className="btn-item" onClick={goHome}>
                <span>{intl.messages.yes}</span>
              </div>
            </div>
          </div>
        </Popup>
      )}
    </>
  );
};

export default Header;
