import React, { useState, useEffect, Suspense } from 'react';
import './App.css';
import './reset.scss';
import { Switch } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import Footer from './components/footer/footer';

import Routes from './router/Router';
import { useDispatch, useSelector } from 'react-redux';
import { auth_me_req } from './api/user-login/user.login.api';
import { getCookie } from './helpers/cookie.helpers';
import { IntlProvider } from 'react-intl';
import ScrollToTop from './app-components/scrolltotop/scrollToTop';

import { getWishesList_req } from './api/wish/wish.api';
import Header from './components/header/header';

const getLocaleState = () => {
  const localization = getCookie('locale');
  if (!localization) {
    return 'am';
  } else {
    return localization;
  }
};

const App = () => {
  const [wish, setWish] = useState([]);
  const state = useSelector((state) => state.lang);
  const user = useSelector((state) => state.user.data);

  const dispatch = useDispatch();
  const token = localStorage.token;
  const locale = getLocaleState();
  const userToken = async () => {
    try {
      if (token) {
        const authForm = await auth_me_req(token);
        dispatch({ type: 'USER_REDUCER', authForm });
      }
    } catch (e) {
      console.log('e', e.response);
    }
  };
  useEffect(() => {
    const WishesList = async () => {
      try {
        if (user) {
          const Wish = await getWishesList_req();
          if (Wish && Wish.data) {
            dispatch({ type: 'FAVORITES_ADD', payload: Wish });
            setWish(Wish);
          }
        }
      } catch (e) {
        console.log('e', e.response);
      }
    };
    WishesList();
  }, []);
  useEffect(() => {
    // if (user) {
    //   userToken();
    // }
    userToken();
  }, []);

  return (
    <IntlProvider
      locale={locale}
      messages={require(`./localization/${locale}.json`)}
    >
      <div className="app-wrapper">
        <ScrollToTop />
        <Header token={token} />
        <Suspense fallback={<div>Loading...</div>}>
          <main role="main" className="main-container">
            <Switch>{renderRoutes(Routes)}</Switch>
          </main>
        </Suspense>

        <Footer />
      </div>
    </IntlProvider>
  );
};

export default App;
