import * as axios from 'axios';
import { axiosAuthInstance } from './auth.config';

const instance = axios.create({
  baseURL: 'https://mylot.am/api/products',
});
const instanceProject = axios.create({
  baseURL: 'https://mylot.am/api/',
});

export const authApi = {
  verify(token) {
    localStorage.setItem('verify', token);
    return instanceProject.post(`auth/verify/${token}`);
  },
  verifyMe(verify) {
    return instanceProject.get(`auth/me`, {
      headers: {
        Authorization: 'Bearer ' + verify,
      },
    });
  },
};

export const AddProductAPI = {
  filterGroup(id) {
    return instanceProject
      .get(`/filter-group/category/${id}`)
      .then((response) => {
        return response.data;
      });
  },
  addProduct(payload) {
    const formData = new FormData();
    formData.append('images[]', [payload.images]);
    return axiosAuthInstance
      .post(
        `/products?title=${payload.title}
                                                     &description=${payload.description}
                                                     &status_of_product=${payload.statusProduct}
                                                     &start_price=${payload.startPrice}
                                                     &auction_type=${payload.auctionType}
                                                     &start_date=2020-07-24 22:00:00
                                                     &end_date=2020-07-25 21:00:00
                                                     &category=${payload.category}
                                                     &region_id=${payload.region_id}
                                                     &city_id=1
                                                     &min_bid_price=${payload.minBidPrice}
                                                      `,
        formData
      )
      .then((response) => {
        return response.data;
      });
  },
};

export const ProductsAPI = {
  getUrgenthyProducts() {
    return instance.get('/upcoming_auctions?type=paginate').then((response) => {
      return response.data;
    });
  },
  getTopProducts() {
    return instance.get('/upcoming_auctions?type=paginate').then((response) => {
      return response.data;
    });
  },
  getUpcomingProducts() {
    return instance.get('/upcoming_auctions?type=paginate').then((response) => {
      return response.data;
    });
  },
  getLiveProducts() {
    return instance.get('/live_auctions?type=paginate').then((response) => {
      return response.data;
    });
  },
  getLastProducts() {
    return instance.get('/last_chanse?type=paginate').then((response) => {
      return response.data;
    });
  },
  getProductPage(productId) {
    return axiosAuthInstance.get(`/products/${productId}`).then((response) => {
      return response.data;
    });
  },
};

export const getProductPage_req = async (productId) => {
  const response = await axiosAuthInstance.get(`/products/${productId}`);
  return response.data;
};
