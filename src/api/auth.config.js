import axios from 'axios';

import { getCookie } from '../helpers/cookie.helpers';
const baseURL = 'https://mylot.am/api';
const coockieData = getCookie('locale');
export const axiosAuthInstance = axios.create({
  baseURL: baseURL,
  timeout: 60000,
});

// export const axiosAuthInstance = axios.create({
// 	baseURL: baseURL,
//     timeout: 60000,
//     headers: {
//         Authorization: 'Bearer ' + localStorage.token,
//         'Content-Type':'multipart/form-data'
//      }
// });

const successResponse = (response) => response;
const errorResponse = (error) => {
  return Promise.reject(error);
};
const setHeaders = (reqConfig) => {
  // reqConfig.headers['Content-Type'] = 'application/json';
  const token = localStorage.token;
  if (token) {
    reqConfig.headers.Authorization = 'Bearer ' + token;
    reqConfig.headers.ContentType = 'multipart/form-data';
    reqConfig.headers.language =
      coockieData === null ? 'am' : getCookie('locale');
  }

  return reqConfig;
};

axiosAuthInstance.interceptors.response.use(successResponse, errorResponse);
axiosAuthInstance.interceptors.request.use(setHeaders);
