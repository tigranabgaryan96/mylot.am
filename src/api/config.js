import axios from 'axios';
import { getCookie } from '../helpers/cookie.helpers';
const baseURL = 'https://mylot.am/api';

const coockieData = getCookie('locale');

export const axiosInstance = axios.create({
  baseURL: baseURL,
  timeout: 60000,
});

const successResponse = (response) => {
  return response;
};

const errorResponse = (error) => {
  return Promise.reject(error);
};
const setHeaders = (reqConfig) => {
  // reqConfig.headers['Content-Type'] = 'application/json';

  reqConfig.headers.language =
    coockieData === null ? 'am' : getCookie('locale');

  return reqConfig;
};

axiosInstance.interceptors.response.use(successResponse, errorResponse);
axiosInstance.interceptors.request.use(setHeaders);
