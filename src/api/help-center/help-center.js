import { axiosInstance } from '../config';
export const short_req = async () => {
  const response = await axiosInstance.get(`/pages/short`);
  return response.data;
};
export const commision_req = async () => {
  const response = await axiosInstance.get(`/pages/commission`);
  return response.data;
};

export const aboutCommision_req = async () => {
  const response = await axiosInstance.get(`/pages/about-commission`);
  return response.data;
};
export const HowToUse_req = async () => {
  const response = await axiosInstance.get(`/pages/how-to-use`);
  return response.data;
};

export const AfterVictory_req = async () => {
  const response = await axiosInstance.get(`/pages/after-the-victory`);
  return response.data;
};

export const MessagesBenefits_req = async () => {
  const response = await axiosInstance.get(
    `/pages/messages-and-other-benefits`
  );
  return response.data;
};

export const Foreword_req = async () => {
  const response = await axiosInstance.get(`/pages/foreword`);
  return response.data;
};
export const Important_req = async () => {
  const response = await axiosInstance.get(`/pages/important`);
  return response.data;
};
