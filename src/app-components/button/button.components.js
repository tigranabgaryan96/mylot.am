import React from "react";
import "./button.components.scss";

const ButtonComponent = ({ text, _onClick = () => {}, className } ) => {
  return (
    <button className={`button-click  ${className} `} onClick={_onClick}>
      {text && <span>{text}</span>}
    </button>
  );
};

export default ButtonComponent;
