import React from 'react';
import { Link } from 'react-router-dom';
import './user-product.scss';
import '../../reset.scss';
import { useIntl } from 'react-intl';
import { Config } from '../../constants/config';
// import DeleteModal from "../../components/modal-popup/delete-modal/delete-modal";
// import { useHistory } from "react-router-dom";

const UserProduct = (props) => {
  const intl = useIntl();
  // let history = useHistory();

  // function handleClick() {
  //   history.push(`/product-page/${props.id}`);
  // }

  return (
    <div className="user-product">
      <Link to={`/product-page/${props.id}`}>
        <div className="user-product-img">
          <img
            src={
              Config?.ImageUrl +
              props?.image?.cover +
              '_mediumOne.' +
              props.image?.ext
            }
            alt="#"
          />
        </div>
      </Link>
      <div className="user-product-content">
        <h4>{props.title}</h4>
        <div className="user-product-text">
          <p>{intl.messages.product_info.lot_id_name}</p>
          <span>{props.id}</span>
        </div>
        <div className="user-product-text">
          <p>{intl.messages.add_item.step_two_start_step}</p>
          <span>
            {intl.formatNumber(props.startPrice)} {props.currency}
          </span>
        </div>
        {props.min_bid_price && (
          <div className="user-product-text">
            <p>{intl.messages.add_item.step_two_minimum_step}</p>
            <span>{intl.formatNumber(props.min_bid_price)}</span>
            <span>{intl.messages.money}</span>
          </div>
        )}
        {props.bidPrice && (
          <div className="user-product-text">
            <p>{intl.messages.lot.buy_now_price}</p>
            <span>{intl.formatNumber(props.bidPrice)}</span>
            <span>{intl.messages.money}</span>
          </div>
        )}
        {props.start_price && (
          <div className="user-product-text">
            <p>{intl.messages.add_item.step_two_start_step}</p>
            <span>{intl.formatNumber(props.start_price)}</span>
            <span>{intl.messages.money}</span>
          </div>
        )}
        {props.highest_suggestion && (
          <div className="user-product-text">
            <p>{intl.messages.lot.highest_suggestion}</p>
            <span>{intl.formatNumber(props.highest_suggestion)}</span>
            <span>{intl.messages.money}</span>
          </div>
        )}
        {/* {props.highest_suggestion && (
          <div className="user-product-text">
            <p>{intl.messages.participants}</p>
            <span>{props.participators}</span>
          </div>
        )} */}
        {props.participators ? (
          <div className="user-product-text">
            <p>{intl.messages.participants}</p>
            <span>{props.participators}</span>
          </div>
        ) : null}
        {props.startDate ? (
          <div className="user-product-text">
            <p>{intl.messages.product_info.start_date_text}</p>
            <span>{props.startDate}</span>
          </div>
        ) : null}
        {props.endDate ? (
          <div className="user-product-text">
            <p>{intl.messages.product_info.end_date_text}</p>
            <span>{props.endDate}</span>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default UserProduct;
