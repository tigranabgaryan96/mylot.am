/* App Style. */

@font-face {
  font-family: 'Montserrat';
  src: url('./assets/fonts/Montserrat-Regular.eot');
  src: url('./assets/fonts/Montserrat-Regular.eot?#iefix')
      format('embedded-opentype'),
    url('./assets/fonts/Montserrat-Regular.woff2') format('woff2'),
    url('./assets/fonts/Montserrat-Regular.woff') format('woff'),
    url('./assets/fonts/Montserrat-Regular.ttf') format('truetype');
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Trajan Pro 3';
  src: url('./assets/fonts/TrajanPro3-Regular.eot');
  src: url('./assets/fonts/TrajanPro3-Regular.eot?#iefix')
      format('embedded-opentype'),
    url('./assets/fonts/TrajanPro3-Regular.woff2') format('woff2'),
    url('./assets/fonts/TrajanPro3-Regular.woff') format('woff'),
    url('./assets/fonts/TrajanPro3-Regular.ttf') format('truetype');
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Open Sans';
  src: url('./assets/fonts/OpenSans.eot');
  src: url('./assets/fonts/OpenSans.eot?#iefix') format('embedded-opentype'),
    url('./assets/fonts/OpenSans.woff2') format('woff2'),
    url('./assets/fonts/OpenSans.woff') format('woff'),
    url('./assets/fonts/OpenSans.ttf') format('truetype');
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Open Sans';
  src: url('./assets/fonts/OpenSans-Bold.eot');
  src: url('./assets/fonts/OpenSans-Bold.eot?#iefix')
      format('embedded-opentype'),
    url('./assets/fonts/OpenSans-Bold.woff2') format('woff2'),
    url('./assets/fonts/OpenSans-Bold.woff') format('woff'),
    url('./assets/fonts/OpenSans-Bold.ttf') format('truetype');
  font-weight: bold;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Open Sans';
  src: url('./assets/fonts/OpenSans-Semibold.eot');
  src: url('./assets/fonts/OpenSans-Semibold.eot?#iefix')
      format('embedded-opentype'),
    url('./assets/fonts/OpenSans-Semibold.woff2') format('woff2'),
    url('./assets/fonts/OpenSans-Semibold.woff') format('woff'),
    url('./assets/fonts/OpenSans-Semibold.ttf') format('truetype');
  font-weight: 600;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Segoe UI';
  src: url('./assets/fonts/SegoeUI.eot');
  src: url('./assets/fonts/SegoeUI.eot?#iefix') format('embedded-opentype'),
    url('./assets/fonts/SegoeUI.woff2') format('woff2'),
    url('./assets/fonts/SegoeUI.woff') format('woff'),
    url('./assets/fonts/SegoeUI.ttf') format('truetype');
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

/* Class Font Family */

.Sans_SemiBold {
  font-family: 'Open Sans';
  font-weight: 600;
}
.Sans_Bold {
  font-family: 'Open Sans';
  font-weight: bold;
}
.Sans_Regular {
  font-family: 'Open Sans';
  font-weight: 400;
}

/*  */

.app-container {
  width: 100%;
  padding: 0 15px;
}
.liveAuctionContainer {
  padding: 0 15px;
  margin: 0 auto;
  box-sizing: border-box;
  margin-top: 10px;
}
.UserItemContainer {
  padding: 0 15px;
  margin: 0 auto;
  box-sizing: border-box;
  float: left;
}
.clearfix:after {
  content: '';
  clear: both;
  display: table;
}
ol,
ul {
  list-style: none;
  padding-left: 0;
  margin: 0;
}
blockquote,
q {
  quotes: none;
}
blockquote:before,
blockquote:after,
q:before,
q:after {
  content: none;
}
:focus,
.focus {
  outline: 0;
  box-shadow: none;
}
.btn.focus,
.btn:focus {
  box-shadow: none;
}
button:focus {
  outline: none;
}
.btn:hover {
  color: #fdfdfd;
  text-decoration: none;
}
ins {
  text-decoration: none;
}
del {
  text-decoration: line-through;
}
table {
  border-spacing: 0;
  border-collapse: collapse;
}
/*.card {
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
}*/
img {
  border: 0;
}
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td {
  font-size: 100%;
  background: transparent;
}
.clear {
  clear: both;
}
a:active,
a:focus,
a:hover {
  border: none;
  outline: none;
  text-decoration: none;
}
input:active,
input:focus {
  outline: none;
}
input {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
a {
  text-decoration: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}
hr {
  border: none;
  margin: 0;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}

/* Fonts */

.font-12 {
  font-size: 12px;
}
.font-14 {
  font-size: 14px;
}
.font-16 {
  font-size: 16px;
}
.font-18 {
  font-size: 18px;
}
.font-20 {
  font-size: 20px;
}
.font-24 {
  font-size: 24px;
}
.font-26 {
  font-size: 26px;
}
/* Width */

.h-40 {
  height: 40px;
}

.addProduct_input {
  width: 100%;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  padding: 10px;
  letter-spacing: 1px;
  font-size: 14px;
  font-weight: 400;
}
.addProduct_input:focus {
  border: 1px solid #1e88e5;
}

/* Color  */

.lot-color {
  color: #1890ff;
}

.time-danger {
  color: #c62828;
}
.header_blue {
  color: #1e88e5;
  font-size: 14px;
}
.header_blues {
  color: #3d90ce;
  font-size: 21px;
}

/* Component Navbar Style */

/* Add Item Component Style */

.ant-tabs-nav-scroll {
  display: flex;
  justify-content: center;
}

/* carousel */

.BrainhubCarousel__arrows {
  background-color: #ffffff !important;
}
.BrainhubCarousel__arrows span {
  border-color: #0288d1 !important;
}
.modal-content {
  background: #f4f6f7;
}

/*  Nav Tab  */

.nav-tabs .nav-link {
  text-align: center;
  color: #000000;
}
.nav-tabs .nav-item.show .nav-link,
.nav-tabs .nav-link.active {
  border-bottom: none !important;
  color: #0288d1;
}
.ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
  background-color: transparent !important;
  border: none !important;
  color: #ffffff !important;
}
.ant-select-focused {
  border-color: transparent !important;
}
.ant-select-selector {
  background-color: transparent;
}
.ant-dropdown {
  width: 150px;
}
.ant-dropdown-menu-item,
.ant-dropdown-menu-submenu-title {
  line-height: 26px;
}
.ant-drawer-left.ant-drawer-open .ant-drawer-content-wrapper {
  width: 320px !important;
}
.category_block .ant-menu-item-group-title {
  padding: 0;
}

/* Scroll style */
/*body::-webkit-scrollbar {
  width: 0.7em;
}
body::-webkit-scrollbar-track {
  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.2);
}
body::-webkit-scrollbar-thumb {
  background-color: #3d90ce;
  outline: 1px solid #3d90ce;
}*/

.nav-tabs {
  border: none;
}
.nav-link {
  border: 1px solid transparent !important;
  box-sizing: border-box;
}
.nav-link.active {
  border: 1px solid #dee2e6 !important;
  border-bottom: none !important;
  font-weight: bold;
  z-index: 1;
}
.nav-link:hover {
  border: none;
}
.nav-link.active:hover {
  border: none;
}

.not_found_text {
  color: #3d90ce;
  font-size: 60px;
  margin-bottom: 0;
  padding: 50px 0;
  min-height: calc(100vh - 300px);
}

@media screen and (min-width: 360px) and (max-width: 559px) {
  .carousel-inner {
    height: 270px;
  }
  .coming_soon .content .section_text p {
    font-size: 4rem;
  }
  .navbar-custom {
    padding: 0 10px;
  }
  .nav-tabs .nav-link {
    width: 120px;
  }
}

@media screen and (min-width: 560px) and (max-width: 719px) {
  .carousel-inner {
    height: 380px;
  }
  .navbar-custom {
    padding: 0 15px;
  }
  .nav-tabs .nav-link {
    width: 140px;
  }
}

@media screen and (min-width: 720px) and (max-width: 959px) {
  .carousel-inner {
    height: 450px;
  }
  .navbar-custom {
    padding: 0 15px;
  }
  .nav-tabs .nav-link {
    width: 140px;
  }
}

@media screen and (min-width: 960px) and (max-width: 1139px) {
  .carousel-inner {
    height: 530px;
  }
  .navbar-custom {
    padding: 0 15px;
  }
  .nav-tabs .nav-link {
    width: 140px;
  }
}

@media screen and (min-width: 1140px) and (max-width: 1599px) {
  .carousel-inner {
    height: 560px;
  }
  .navbar-custom {
    padding: 0 20px;
  }
  .nav-tabs .nav-link {
    width: 140px;
  }
}

@media (min-width: 1600px) {
  .container,
  .container-lg,
  .container-md,
  .container-sm,
  .container-xl {
    max-width: 1400px;
  }
  .carousel-inner {
    height: 470px;
  }
  .navbar-custom {
    padding: 0 20px;
  }
  .nav-tabs .nav-link {
    width: 140px;
  }
}
@media (max-width: 992px) {
  .not_found_text {
    font-size: 40px;
  }
}
@media (max-width: 480px) {
  .not_found_text {
    font-size: 30px;
  }
}
