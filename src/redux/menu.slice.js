import { createSlice } from '@reduxjs/toolkit';

export const menuSlice = createSlice({
  name: 'menu',
  initialState: {
    currentMenu: false,
  },
  reducers: {
    changeMenu: (state, action) => {
      state.currentMenu = !state.currentMenu;
    },
  },
});

export const { changeMenu } = menuSlice.actions;

export default menuSlice.reducer;
