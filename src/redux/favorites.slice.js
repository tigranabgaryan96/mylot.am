import { createSlice } from '@reduxjs/toolkit';

export const favoritesSlice = createSlice({
  name: 'favoritesList',
  initialState: {
    favorites: [],
  },
  reducers: {
    favoritesProductList: (state, action) => {
      state.favorites = action;
    },
  },
});

export const { favoritesProductList } = favoritesSlice.actions;

export default favoritesSlice.reducer;
