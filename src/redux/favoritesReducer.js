const FAVORITES_REDUCER = 'FAVORITES_REDUCER';
const FAVORITES_ADD = 'FAVORITES_ADD';

let initialState = {};

const FavoritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FAVORITES_REDUCER:
      return {
        ...state,
        ...action.deleteProductReq,
      };
    case FAVORITES_ADD:
      return {
        ...state,
        ...action,
      };
    default:
      return state;
  }
};

export default FavoritesReducer;
