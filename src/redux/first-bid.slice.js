import { createSlice } from '@reduxjs/toolkit';

export const firstBidSlice = createSlice({
  name: 'firstBid',
  initialState: {
    firstBid: false,
  },
  reducers: {
    changeBid: (state, action) => {
      state.firstBid = true;
    },
  },
});

export const { changeBid } = firstBidSlice.actions;

export default firstBidSlice.reducer;
