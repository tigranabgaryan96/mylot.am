import { createSlice } from '@reduxjs/toolkit';

export const addProductSlice = createSlice({
  name: 'addProduct',
  initialState: {
    category: '',
    data: {},
  },
  reducers: {
    addProductCategory: (state, action) => {
      state.category = action.payload;
    },
    finishData: (state, action) => {
      state.data = action.payload;
    },
  },
});

export const { addProductCategory, finishData } = addProductSlice.actions;

export default addProductSlice.reducer;
