import React from 'react';
import ProductPage from '../components/product-page/product-page';
import VerifyEmailToken from '../components/verify-email/verify-email';

const ContainerAuction = React.lazy(() =>
  import('../components/container-auction/container-auction')
);
const Registration = React.lazy(() =>
  import('../components/user-registration/user-registration')
);
const Login = React.lazy(() => import('../components/user-login/user-login'));
const AllItems = React.lazy(() => import('../components/all-items/all-items'));
const UserAuction = React.lazy(() =>
  import('../components/user-auction/user-auction')
);
const AddItem = React.lazy(() => import('../components/add-item/add-item'));
const AboutUs = React.lazy(() => import('../components/about-us/about-us'));
const UserPurchases = React.lazy(() =>
  import('../components/user-purchases/user-purchases')
);
const UserSettings = React.lazy(() =>
  import('../components/user-settings/user-settings')
);
const AuctionItem = React.lazy(() =>
  import('../components/all-items/all-items')
);
const EditItem = React.lazy(() => import('../components/edit-item/edit-item'));
const ContactUs = React.lazy(() =>
  import('../components/contact-us/contact-us')
);

const VerifyEmail = React.lazy(() =>
  import('../components/verify-email/verify-email')
);
const ExpectedViewMore = React.lazy(() =>
  import(
    '../components/container-auction/view-more/expected/expected-view-more'
  )
);
const LiveViewMore = React.lazy(() =>
  import(
    '../components/container-auction/view-more/live-view-more/live-view-more'
  )
);
const FavoritesContainer = React.lazy(() =>
  import('../components/favorites/favorites-container')
);
const NotFound = React.lazy(() => import('../components/not-found/not-found'));
const HelpCenter = React.lazy(() =>
  import('../components/help-center/help-center')
);
const AuctionContainer = React.lazy(() =>
  import('../components/container-auction/auction-container')
);
const SearchPage = React.lazy(() =>
  import('../components/search-page/search-page')
);
const ForgotPassword = React.lazy(() =>
  import('../components/forgot-password/forgot-password')
);
const ResetPasswordContainer = React.lazy(() =>
  import('../components/reset-password/reset-password-container')
);
const LastViewMore = React.lazy(() =>
  import(
    '../components/container-auction/view-more/last-view-more/last-view-more'
  )
);
const Payment = React.lazy(() => import('../components/payment/payment'));
const ResetPassword = React.lazy(() =>
  import('../components/reset-password/reset-password')
);
const Politics = React.lazy(() => import('../components/politics/politics'));

const Routes = [
  {
    path: '/',
    exact: true,
    component: ContainerAuction,
  },
  {
    path: '/auction',
    component: AuctionContainer,
  },
  {
    path: '/politics',
    component: Politics,
  },
  {
    path: '/login',
    component: Login,
  },
  {
    path: '/registration',
    component: Registration,
  },
  {
    path: '/allItems',
    component: AllItems,
  },
  {
    path: '/my-auction',
    component: UserAuction,
  },
  {
    path: '/addItem',
    component: AddItem,
  },
  {
    path: '/about_us',
    component: AboutUs,
  },
  {
    path: '/home',
    component: ContainerAuction,
  },
  {
    path: '/favoritesContainer',
    component: FavoritesContainer,
  },
  {
    path: '/purchases',
    component: UserPurchases,
  },
  {
    path: '/usersettings',
    component: UserSettings,
  },
  {
    path: `/slug/:slug`,
    component: AuctionItem,
  },
  {
    path: '/edititem/:id',
    component: EditItem,
  },
  {
    path: '/contact_us',
    component: ContactUs,
  },
  {
    path: '/verify',
    component: VerifyEmailToken,
  },
  {
    path: '/verifyEmail',
    component: VerifyEmail,
  },
  {
    path: '/product-page',
    component: ProductPage,
  },
  {
    path: '/expected',
    component: ExpectedViewMore,
  },
  {
    path: '/liveView',
    component: LiveViewMore,
  },
  {
    path: '/lastView',
    component: LastViewMore,
  },
  {
    path: '/help-center',
    component: HelpCenter,
  },
  {
    path: '/search-page',
    component: SearchPage,
  },
  {
    path: '/forgot-password',
    component: ForgotPassword,
  },
  {
    path: '/reset-password',
    component: ResetPasswordContainer,
  },

  {
    path: '/payment/',
    component: Payment,
  },
  {
    path: '/api/auth/change-password/',
    component: ResetPassword,
  },
  {
    component: NotFound,
  },
];

export default Routes;
