import { Config } from "../constants/config";

export const pictureMapper = (cover) => ({cover}); 

export const picutureUrlMapper = ({ cover, ext }, size = "_mediumOne") => ({
  cover: `${Config.ImageUrl}${cover}${size}.${ext}`,
});
